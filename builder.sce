// Copyright (C) 2008 - INRIA
// Copyright (C) 2009-2011 - DIGITEO

// This file is released under the 3-clause BSD license. See COPYING-BSD.

mode(-1);
lines(0);

function main_builder()

	TOOLBOX_NAME  = "CPGE";
	TOOLBOX_TITLE = "CPGE - Xcos toolbox";
	toolbox_dir   = get_absolute_file_path("builder.sce");

	// Check Scilab's version
	// =============================================================================

	// check minimal version (xcosPal required)
	if ~isdef("xcosPal") then
		// and xcos features required
		error(gettext('Scilab 5.4 or more is required.'));
	end

	try
		v = getversion("scilab");
	catch
		error(gettext("Scilab 5.4 or more is required."));
	end

	if v(1) < 6 & v(2) < 0 then
		// new API in scilab 5.3
		error(gettext("Scilab 5.4 or more is required."));
	end

	// Check modules_manager module availability
	// =============================================================================

	if ~isdef("tbx_build_loader") then
		error(msprintf(gettext("%s module not installed."), "modules_manager"));
	end

	// Action
	// =============================================================================

	tbx_builder_macros(toolbox_dir);
	//tbx_builder_src(toolbox_dir);
	//tbx_builder_gateway(toolbox_dir);
	tbx_builder_help(toolbox_dir);
	tbx_build_loader(toolbox_dir);
	tbx_build_cleaner(toolbox_dir);
endfunction

if with_module('xcos') then
	main_builder();
	clear main_builder; // remove main_builder on stack
end
