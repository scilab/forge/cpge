// ============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Bruno JOFRET
//
//  This file is distributed under the same license as the Scilab package.
// ============================================================================
//
// <-- ENGLISH IMPOSED -->
//
// <-- Short Description -->
// SUMMATION

loadXcosLibs();

// Check where SUMMATION block is defined
assert_checkequal(whereis("SUMMATION"), "Linearlib");
[a, b] = libraryinfo(whereis("SUMMATION"));
assert_checkequal(b, strsubst("SCI/modules/scicos_blocks/macros/Linear/", "/", filesep()));

funcprot(0);
needcompile = 0;
alreadyran = %f;
%scicos_context = struct();

[status, message] = xcosValidateBlockSet("SUMMATION");
if status == %f
    disp(message);
end
assert_checktrue(status);

disp(SUMMATION("define"));