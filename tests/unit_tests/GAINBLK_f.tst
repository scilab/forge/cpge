// ============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Bruno JOFRET
//
//  This file is distributed under the same license as the Scilab package.
// ============================================================================
//
// <-- ENGLISH IMPOSED -->
//
// <-- Short Description -->
// GAINBLK_f

loadXcosLibs();

// Check where GAINBLK_f block is defined
assert_checkequal(whereis("GAINBLK_f"), "Linearlib");
[a, b] = libraryinfo(whereis("GAINBLK_f"));
assert_checkequal(b, strsubst("SCI/modules/scicos_blocks/macros/Linear/", "/", filesep()));

funcprot(0);
needcompile = 0;
alreadyran = %f;
%scicos_context = struct();

[status, message] = xcosValidateBlockSet("GAINBLK_f");
if status == %f
    disp(message);
end
assert_checktrue(status);

disp(GAINBLK_f("define"));