// ============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Bruno JOFRET
//
//  This file is distributed under the same license as the Scilab package.
// ============================================================================
//
// <-- ENGLISH IMPOSED -->
//
// <-- Short Description -->
// Test Exemple livret
[a, cpgeMacrosPath] = libraryinfo("cpgelib");

assert_checktrue(importXcosDiagram(cpgeMacrosPath + "/../examples/Livret_CPGE_Exemple1.zcos"));

xcos_simulate(scs_m, 4);

// Une fenetre doit s'ouvrir
assert_checktrue(~isempty(winsid()));

// TODO : Check des donnees dans la fenetre