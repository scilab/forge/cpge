// ============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Bruno JOFRET
//
//  This file is distributed under the same license as the Scilab package.
// ============================================================================
//
// <-- ENGLISH IMPOSED -->
//
// <-- Short Description -->
// SATURATION

loadXcosLibs();

// Check where SATURATION block is defined
assert_checkequal(whereis("SATURATION"), "Nonlinearlib");
[a, b] = libraryinfo(whereis("SATURATION"));
assert_checkequal(b, strsubst("SCI/modules/scicos_blocks/macros/NonLinear/", "/", filesep()));

funcprot(0);
needcompile = 0;
alreadyran = %f;
%scicos_context = struct();

[status, message] = xcosValidateBlockSet("SATURATION");
if status == %f
    disp(message);
end
assert_checktrue(status);

disp(SATURATION("define"));