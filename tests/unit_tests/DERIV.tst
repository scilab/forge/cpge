// ============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Bruno JOFRET
//
//  This file is distributed under the same license as the Scilab package.
// ============================================================================
//
// <-- ENGLISH IMPOSED -->
//
// <-- Short Description -->
// DERIV

loadXcosLibs();

// Check where DERIV block is defined
assert_checkequal(whereis("DERIV"), "Linearlib");
[a, b] = libraryinfo(whereis("DERIV"));
assert_checkequal(b, strsubst("SCI/modules/scicos_blocks/macros/Linear/", "/", filesep()));


funcprot(0);
needcompile = 0;
alreadyran = %f;
%scicos_context = struct();

[status, message] = xcosValidateBlockSet("DERIV");
if status == %f
    disp(message);
end
assert_checktrue(status);

disp(DERIV("define"));