// ============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Bruno JOFRET
//
//  This file is distributed under the same license as the Scilab package.
// ============================================================================
//
// <-- ENGLISH IMPOSED -->
//
// <-- Short Description -->
// TRAPEZOID

loadXcosLibs();

// Check where TRAPEZOID block is defined
assert_checkequal(whereis("TRAPEZOID"), "cpgelib");

funcprot(0);
needcompile = 0;
alreadyran = %f;
%scicos_context = struct();

[status, message] = xcosValidateBlockSet("TRAPEZOID");
if status == %f
    disp(message);
end
assert_checktrue(status);

disp(TRAPEZOID("define"));
