// Copyright (C) 2011 - DIGITEO - Bruno JOFRET
// Copyright (C) 2013 - Scilab Enterprises - Bruno JOFRET
//
// This file is released under the 3-clause BSD license. See COPYING-BSD.

function subdemolist = demo_gateway()

  demopath = get_absolute_file_path("cpge.dem.gateway.sce");
  subdemolist = ["Asservissement MAXPID", "MAXPID_Asservi.dem.sce" ;
                 "Asservissement MAXPID fréquentiel", "MAXPID_Asservi_bode.dem.sce" ;
                "Moteur Courant Continu (modèle fin)", "MCC_modele_fin.dem.sce";
                "Moteur Courant Continu (Bode)", "MCC_developpe_bode.dem.sce";
                "Moteur Courant Continu (Param)", "MCC_developpe_param.dem.sce";
                "Moteur Courant Continu (Param+Bode)", "MCC_developpe_bode_param.dem.sce";
                "Pilotage Imprimante", "pilotage_imprimante.dem.sce";
                "Exemples Livret", "exemplesLivret.dem.gateway.sce";
                ]; // add demos here
  subdemolist(:,2) = demopath + subdemolist(:,2);

endfunction

subdemolist = demo_gateway();
clear demo_gateway; // remove demo_gateway on stack
