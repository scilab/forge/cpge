// Copyright (C) 2013 - Scilab Enterprises - Bruno JOFRET
//
// This file is released under the 3-clause BSD license. See COPYING-BSD.

function subdemolist = demo_gateway()

  demopath = get_absolute_file_path("exemplesLivret.dem.gateway.sce");
  subdemolist = ["Exemple 1", "Exemple1.dem.sce" ;
                 "Exemple 2", "Exemple2.dem.sce" ;
                 "Exemple 3", "Exemple3.dem.sce" ;
                 "Exemple 4", "Exemple4.dem.sce" ;
                 "Exemple 5", "Exemple5.dem.sce" ;
                ]; // add demos here
  subdemolist(:,2) = demopath + subdemolist(:,2);

endfunction

subdemolist = demo_gateway();
clear demo_gateway; // remove demo_gateway on stack
