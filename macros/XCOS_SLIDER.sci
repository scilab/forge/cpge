//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x, y, typ]=XCOS_SLIDER(job, arg1, arg2)
    x=[];
    y=[];
    typ=[];
    select job
     case 'plot' then
// deprecated
     case 'getinputs' then
// deprecater
     case 'getoutputs' then
// deprecated
     case 'getorigin' then
// deprecated
     case 'set' then
      x=arg1;
        graphics=arg1.graphics;
        exprs=graphics.exprs
        model=arg1.model;
        while %t do
            [ok,name,value_max,value_min,initial_value,step,exprs] = scicos_getvalue('Paramètres du slider',..
                                                ['Nom';'Valeur maximale';'Valeur minimale';'Valeur initiale';'Nombre d''incréments '], ..
                                                list('str',-1,'vec',-1,'vec',-1,'vec',-1,'vec',-1),exprs(1:5));
            if ~ok then break end
            if value_max<= value_min then
                mess=['La valeur maximale doit être plus grande que la valeur minimale']
                ok=%f
            end
            if initial_value > value_max | initial_value < value_min then
                mess=['La valeur initiale doit être comprise entre les valeurs maximale et minimale']
                ok=%f
            end
            
            if ok then
                model.rpar=[value_max,value_min,initial_value,step,1]
                model.opar=list(name)
                graphics.exprs=exprs
                x.graphics=graphics;
                x.model=model
                break
            else
                message(mess)
            end
        end      
      
      
     case 'define' then
      model=scicos_model();
      state_button=1;
      model.rpar=[1,0,0,100]
      model.sim=list("XCOS_SLIDER_sim", 5)
      model.blocktype='c';
      model.dep_ut=[%f %t];
      model.out=[1];
      model.evtin=[1];
      model.firing=1
      x=standard_define([2 2],model,[],[]);
      x.graphics.in_implicit=[];
      x.graphics.out_implicit=['E'];
      x.graphics.exprs=['slider',string(model.rpar)];
      
    end
endfunction
