//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
function freq_analysis(sys,typ)
    fig=gcf();
    immediate_drawing=fig.immediate_drawing;
    fig.immediate_drawing="off";

    if typ=="bode" then
        f=gcf();
        axg=f.children(2);
        axp=f.children(1);
        wmin=min(axg.x_ticks.locations)*2*%pi;
        wmax=max(axg.x_ticks.locations)*2*%pi;
        gmin=min(axg.y_ticks.locations);
        gmax=max(axg.y_ticks.locations);
        pmin=min(axp.y_ticks.locations);
        pmax=max(axp.y_ticks.locations);

        [Mg,fr]=g_margin(sys)
        wr=fr*2*%pi;
        sca(axp);
        xpoly([wmin/(2*%pi);wmax/(2*%pi)],[-180;-180])
        e=gce();e.foreground=color('red');e.line_style=4;
        if wr<>[] then
            xpoly([wr;wr],[pmin;pmax])
            e=gce();e.foreground=color('red');e.line_style=4;
            sca(axg);
            xpoly([wr;wr],[gmin;gmax])
            e=gce();e.foreground=color('red');e.line_style=4;
            xpoly([wr;wr],[-Mg;0])
            e=gce();e.foreground=color('red');e.thickness=2;
            if(Mg >0) signe = -3; else signe=3; end
            e=gce();e.foreground=color('red');e.thickness=2;
            xpoly([wr/1.1;wr],[signe;0]);
            e=gce();e.foreground=color('red');e.thickness=2;
            xpoly([wr;1.1*wr],[0;signe]);
            e=gce();e.foreground=color('red');e.thickness=2;
            Mg=floor(100*Mg)/100;
            //xstring(wmax/100,gmax-15,"Marge de gain : "+string(Mg)+"dB");
            disp_Mg="Marge de gain : "+string(Mg)+"dB";
        else
            sca(axg);
            //xstring(wmax/100,-15,"Marge de gain infinie");
            disp_Mg="Marge de gain infinie";
        end


        [Mphi,fr]=p_margin(sys)
        wr=fr*2*%pi;
        sca(axg);
        xpoly([wmin/(2*%pi);wmax/(2*%pi)],[0;0])
        e=gce();e.foreground=color('blue');e.line_style=4;
        if wr<>[] then
            xpoly([wr;wr],[gmin;gmax])
            e=gce();e.foreground=color('blue');e.line_style=4;
            sca(axp);
            xpoly([wr;wr],[pmin;pmax])
            e=gce();e.foreground=color('blue');e.line_style=4;
            xpoly([wr;wr],[-180;Mphi-180])
            e=gce();e.foreground=color('blue');e.thickness=2;
            //fleches
            if(Mphi >0) signe = -10; else signe=0; end
            xpoly([wr/1.1;wr],[Mphi-175+signe;Mphi-180]);
            e=gce();e.foreground=color('blue');e.thickness=2;
            xpoly([wr;1.1*wr],[Mphi-180;Mphi-175+signe]);
            e=gce();e.foreground=color('blue');e.thickness=2;
            Mphi=floor(100*Mphi)/100;
//            xstring(wmax/100,pmax-20,"Marge de phase : "+string(Mphi)+"°");
                        disp_Mphi="Marge de phase : "+string(Mphi)+"°";
        else
            sca(axp);
//            xstring(wmax/100,pmax-20,"Marge de phase indéfinie");
            disp_Mphi="Marge de phase indéfinie"
        end
        
       f.info_message=disp_Mg+' ; '+disp_Mphi;
       sca(axg)
       //xtitle(disp_Mg+' ; '+disp_Mphi)  
    elseif typ=="black" then

        ax=gca();
        pmin=min(ax.x_ticks.locations);
        gmin=min(ax.y_ticks.locations);

        [gm,fr]=g_margin(sys)
        wr=fr*2*%pi;
        if wr<>[] then
            xpoly([-180;-180],[0;-gm]);
            e=gce();e.foreground=color('red');e.thickness=2;
            gm=floor(100*gm)/100;
            wr=floor(100*wr)/100;
            //fleches
            if(gm >0) signe = 1; else signe=-1; end
            xpoly([-180;-182],[0;0-signe*5]);
            e=gce();e.foreground=color('red');e.thickness=2;
            xpoly([-180;-178],[0;0-signe*5]);
            e=gce();e.foreground=color('red');e.thickness=2;
            //texte
//            text=["Mg(dB) : "+string(gm);"Pulsation(rad/s) : "+string(wr)]
//            xstring(-180,-gm-20,text);
            disp_Mg="Mg : "+string(gm)+" dB à "+string(wr)+" rad/s"
        else
//            xstring(pmin,gmin,"Marge de gain infinie");
            disp_Mg="Marge de gain infinie";
        end
        [phm,fr]=p_margin(sys)
        wr=fr*2*%pi;
        if wr<>[] then
            xpoly([-180;phm-180],[0;0]);
            e=gce();e.foreground=color('blue');e.thickness=2;
            phm=floor(100*phm)/100;
            wr=floor(100*wr)/100;   
            //fleches
            if(phm >0) signe = 1; else signe=-1; end
            xpoly([phm-180-signe*5;phm-180],[2;0]);
            e=gce();e.foreground=color('blue');e.thickness=2;
            xpoly([phm-180-signe*5;phm-180],[-2;0]);
            e=gce();e.foreground=color('blue');e.thickness=2;
            //texte
//            text=["Mphase(°) : "+string(phm);"Pulsation(rad/s) : "+string(wr)]
//            xstring(phm-180,-20,text);
            disp_Mphi="Mphase : "+string(phm)+"° à  "+string(wr)+' rad/s';
        else
             disp_Mphi="Marge de phase indéfinie";           
        end
        //[phm,fr2]=p_margin(sys)

        //    xpoly([min(ax.x_ticks.locations);0],[0;0]);
        //    e=gce();e.foreground=color('blue');e.line_style=4;
        //    if fr<>[] then
        //      if sys.dt=='c' then
        //	f=horner(sys,2*%i*%pi*fr)
        //      else
        //	dt=sys.dt;if dt=='d' then dt=1,end
        //	f=horner(sys,exp(2*%i*%pi*fr*dt))
        //      end
        //      xpoly([real(f);0],[0;0]);
        //      e=gce();e.foreground=color('blue');e.thickness=2;
        //    end
        //    [phm,fr]=p_margin(sys)
        //    //unit circle
        //    t=linspace(0,2*%pi,100);
        //    xpoly(sin(t),cos(t))
        //    e=gce();e.foreground=color('red');e.line_style=4;
        //    if fr<>[] then
        //      t=phm*%pi/180+%pi;
        //      xpoly([cos(t);0],[sin(t);0])
        //      e=gce();e.foreground=color('red');e.thickness=2;
        //    end
        // 
       fig.info_message=disp_Mg+' ; '+disp_Mphi;    
       //xtitle(disp_Mg+' ; '+disp_Mphi)       
    else
        disp("Marges non implantées pour Nyquist")
//        ax=gca();
//        [gm,fr]=g_margin(sys)
//
//        xpoly([min(ax.x_ticks.locations);0],[0;0]);
//        e=gce();e.foreground=color('blue');e.line_style=4;
//        if fr<>[] then
//            if sys.dt=='c' then
//                f=horner(sys,2*%i*%pi*fr)
//            else
//                dt=sys.dt;if dt=='d' then dt=1,end
//                f=horner(sys,exp(2*%i*%pi*fr*dt))
//            end
//            xpoly([real(f);0],[0;0]);
//            e=gce();e.foreground=color('blue');e.thickness=2;
//        end
//        [phm,fr]=p_margin(sys)
//        //unit circle
//        t=linspace(0,2*%pi,100);
//        xpoly(sin(t),cos(t))
//        e=gce();e.foreground=color('red');e.line_style=4;
//        if fr<>[] then
//            t=phm*%pi/180+%pi;
//            xpoly([cos(t);0],[sin(t);0])
//            e=gce();e.foreground=color('red');e.thickness=2;
//        end
    end
    fig.immediate_drawing=immediate_drawing; 


endfunction
