//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=PARAM_VAR(job,arg1,arg2)
  x=[];y=[];typ=[];
  select job
   case 'plot' then
// deprecated
   case 'getinputs' then
// deprecater
   case 'getoutputs' then
// deprecated
   case 'getorigin' then
// deprecated
   case 'set' then
    x=arg1;
    graphics=arg1.graphics;
    exprs=graphics.exprs
    model=arg1.model;

// Boite de dialogue pour indiquer les paramètres à faire varier (3 maximum) et leurs valeurs
    while %t do
      [ok,param1,param1val,param2,param2val,param3,param3val, exprs]=scicos_getvalue('Analyse paramétrique',..
                                                ['Nom du 1er paramètre';'Valeurs du 1er parametre';'Nom du 2nd parametre';'Valeurs du 2nd parametre';...
                                                'Nom du 3eme parametre';'Valeurs du 3eme parametre'], ..
                                                list('vec',-1,'vec',-1,'vec',-1,'vec',-1,'vec',-1,'vec',-1),exprs);



//      [ok,param1,param1val,param2,param2val,param3,param3val, exprs]=scicos_getvalue('Varying parameters and their values',..
//                                                ['1st parameter','1st parameter values','2nd parameter','2nd parameter values',...
//                                                '3rd parameter','3rd parameter values'], ..
//                                                list('str',-1,'vec',-1,'str',-1,'vec',-1,'str',-1,'vec',-1),[exprs(1),exprs(2),exprs(3),]);
          mess=[];

          if ~ok then
// Cancel
              break;
          end
            // Verification de l'existence des paramètres à faire varier
            // La verification dans le contexte est faite automatiquement dans le scicos_getvalue

          if ok then
// Everything's ok
              // On a besoin de stocker le nom des variables dans le contexte et plus leur valeur
              // Donc on stocke exprs(1, 3, 5)
              model.opar=list(exprs(1),param1val,exprs(3),param2val,exprs(5),param3val)
              graphics.exprs=exprs;
              x.model=model;
              x.graphics=graphics;
              break;
          else
              message(mess);
          end

end


     case 'define' then
      model=scicos_model();
        k=1;
        model.sim=list("PARAM_VAR",99) // 99 type blocks are ignored by simulator.
        model.blocktype='c';
        model.dep_ut=[%f %f];
        model.in=[];
        model.intyp=[];
        model.out=[];
        model.outtyp=[];
        model.ipar=[1]
        model.rpar=[1;1];
        model.opar=list("k", [1 2 3], "", [], "", [])
        x=standard_define([5 2],model,[],[]);
        x.graphics.in_implicit=[];
        x.graphics.out_implicit=[];
        x.graphics.style=["blockWithLabel;verticalLabelPosition=center;displayedLabel=Param. varation"]
        x.graphics.exprs=["k";"[1 2 3]";"";"";"";""]
    end
endfunction
