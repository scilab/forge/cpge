//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
// Adaptation 2013-2014 - David Fournier / David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=REP_TEMP(job,arg1,arg2)
	x = [];
	y = [];
	typ = [];
	select job
	case 'plot' then
		// deprecated
	case 'getinputs' then
		// deprecater
	case 'getoutputs' then
		// deprecated
	case 'getorigin' then
		// deprecated
	case 'set' then
		x = arg1;
		graphics = arg1.graphics;
		exprs = graphics.exprs;
		model = arg1.model;

		//Choix du type de diagramme
		while (%t) do
			// Compatibilité d'antériorité avec la 1.5.1
			if (size(graphics.exprs,2) < 3) then
				graphics.exprs(3) = "0";
				graphics.exprs(4) = "0";
				graphics.exprs(5) = "0";
			end;

			[ok, num_pts, tf, grid_on, tr, tm, tpic, exprs] = scicos_getvalue(	"Paramètres de l''étude temporelle",..
																						[gettext("Nombre de points"); gettext("Durée de la simulation"); gettext("Grille affichée (2 auto, 1 oui, 0 non)"); gettext("Connaitre le temps de réponse à x% (x oui, -x oui avec visuel, 0 non)"); gettext("Connaitre le temps de montée à x% (x oui, -x oui avec visuel, 0 non)"); gettext("Connaitre le premier dépassement (1 oui, -1 oui avec visuel, 0 non)");], ..
																						list('vec',1,'vec',1,'vec',1,'vec',1,'vec',1,'vec',1), ..
																						[string(model.rpar(1)); string(model.rpar(2)); graphics.exprs(2); graphics.exprs(3); graphics.exprs(4); graphics.exprs(5)]);

			mess = [];

			if ~ok then
				break;
			end;

			if ~(num_pts > 0) then
				mess = [mess ;_("Le nombre de points doit être strictement positif")];
				ok = %f;
			end;

			if ~((grid_on==0) | (grid_on==1) | (grid_on==2)) then
				mess = [mess ;_("Taper 0, 1 ou 2 pour afficher la grille ou non")];
				ok = %f;
			end;

			t_limite = 25;
			if ~((tr >= -t_limite) & (tr <= t_limite)) then
				mess = [mess ;_("Taper un nombre compris entre -" + string(t_limite) + " et " + string(t_limite) + " pour connaitre le temps de réponse ou non")];
				ok = %f;
			end;

			if ~((tm >= -t_limite) & (tm <= t_limite)) then
				mess = [mess ;_("Taper un nombre compris entre -" + string(t_limite) + " et " + string(t_limite) + "pour connaitre le temps de montée ou non")];
				ok = %f;
			end;

			if ~((tpic == -1) | (tpic == 0) | (tpic == 1)) then
				mess = [mess ;_("Taper -1 ou 1 ou 0 pour connaitre le premier dépassement ou non")];
				ok = %f;
			end;

			if ok then
				model.rpar(1) = num_pts;
				model.rpar(2) = tf;
				graphics.exprs = [string(tf), string(grid_on), string(tr), string(tm), string(tpic)];
				x.model = model;
				x.graphics = graphics;
				break;
			else
				message(mess);
			end;
		end;
	case 'define' then
		tf = 100;
		num_pts = 200;
		model = scicos_model();
		model.sim = list("REP_TEMP_sim",99); // 99 type blocks are ignored by simulator.
		model.blocktype = 'c';
		model.dep_ut = [%f %f];
		model.in = [];
		model.intyp = [];
		model.out = [];
		model.outtyp = [];
		model.rpar = [num_pts,tf];
		model.opar = list("E","S");
		x = standard_define([3 2],model,[],[]);
		x.graphics.in_implicit = [];
		x.graphics.out_implicit = [];
		x.graphics.style = ["blockWithLabel;verticalLabelPosition=center;displayedLabel= Time %s s;fillcolor=#FF3333"]
		x.graphics.exprs = [string(tf),"1","0","0","0"];
	end;
endfunction
