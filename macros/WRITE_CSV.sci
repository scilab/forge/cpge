//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x, y, typ]=WRITE_CSV(job, arg1, arg2)
    x=[];
    y=[];
    typ=[];
    select job
     case 'plot' then
// deprecated
     case 'getinputs' then
// deprecater
     case 'getoutputs' then
// deprecated
     case 'getorigin' then
// deprecated
     case 'set' then
      x=arg1;
      graphics=arg1.graphics;exprs=graphics.exprs
      model=arg1.model;
      while %t do
       [ok, path, delimiter, nb_columns, exprs] = scicos_getvalue([msprintf(gettext("Set %s block parameters"), "WRITE_CSV");" "; ..
        gettext("Get data from csv file");" "], ..
        [gettext("Path of csv file, eg:  E:\my path\file.txt"); gettext("delimiter (, or ; or s (space))"); gettext("Number of outputs")], ..
        list("str",-1,"str",-1,"vec",1), exprs);
        if ~ok then break,end
        mess=[];
        if delimiter ~= "s" & delimiter ~= "," & delimiter ~= ";" then
            mess=[mess ;_("Delimiter must be , or ; or s")]
            ok = %f
        end
        if nb_columns<1 then
            mess=[mess ;_("Number of output must be greater than 1")]
            ok = %f
        end
        

        if ok then
          in = ones(nb_columns,1);
          [model,graphics,ok]=set_io(model,graphics,list([in in],in),list(),ones(1,1),[]);
          model.sim=list("WRITE_CSV_sim", 5)
          model.blocktype='d';
          model.dep_ut=[%t %f];
          model.firing=[0;-1]
          model.rpar=[nb_columns, ascii(delimiter)];
          model.ipar=[ascii(path)]
          graphics.exprs=exprs;
          x.graphics=graphics;x.model=model
          break
        end      
      end 
     case 'define' then
      model=scicos_model();
      k=1;
      model.sim=list("WRITE_CSV_sim", 5)
      model.blocktype='d';
      model.dep_ut=[%t %f];
      model.in=[1];
      model.evtin=[1];
      model.firing=[0;-1]
      exprs=string(["test.txt",",","1"])
      model.rpar=[2,ascii(",")];
      model.ipar=[ascii("./test.txt")];
      x=standard_define([4 2],model,exprs,[]);
      x.graphics.out_implicit=[];
      x.graphics.style=["blockWithLabel;verticalLabelPosition=center;displayedLabel= Write csv ;fillcolor=#FF3333"]
      x.graphics.in_implicit=['E'];
      
    end
endfunction
