//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010-2010 - DIGITEO -
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [scs,presence_delay,param_delays,err]=create_new_block_diag(scs_m,input_signal,output_signal)

    err=0; //gestion des erreurs
    scs=scs_m; // On repart sur un diagramme propre
    num_in=[] // le numéro du bloc d'entrée dans le diagramme original
    num_out=[] // Le numéro du bloc de sortie dans le diagramme original
    //Parametres pour la gestion des blocs retard
    param_delays.delay_parameters=[]; //valeur des TIME_DELAY 
    param_delays.num_delay=[]; //stockage des numéros de blocs TIME_DELAY

    param_delays.BO_delay=[]; //flag indiquant si le bloc est dans la BO ou dans une BF   

    objs = scs_m.objs;
    N_objs = size(objs);

    //---------- Traitement des blocs retard TIME_DELAYS-----------
    // On cherche et on remplace les blocs TIME_DELAYS

    for j = 1:N_objs
        curObj= objs(j);
        if (typeof(curObj)=='Block'  & curObj.gui=="TIME_DELAY") then
            presence_delay=%t;
            param_delays.num_delay($+1)=j;
            param_delays.delay_parameters($+1)=curObj.model.rpar(1);
            param_delays.BO_delay($+1)=1;
            //effacement du bloc TIME_DELAY et remplacement par un fct de transfert égale à un gain pur au début
            //                gain=GAINBLK_f('define')
            //                gain.graphics.pin = curObj.graphics.pin;
            //                gain.graphics.pout = curObj.graphics.pout;
            //                objs(j)=gain;
            approx=CLR('define')
            approx.graphics.pin = curObj.graphics.pin;
            approx.graphics.pout = curObj.graphics.pout;
            approx.graphics.exprs=["1";"1"];
            approx.model.rpar=1;
            objs(j)=approx;
        end
    end

    //---------- Traitement de l'entrée -----------
    // On récupère le bloc grandeur physique d'entrée

    for j = 1:N_objs
        curObj= objs(j);
        if (typeof(curObj) == "Block" & curObj.gui == "GRANDEUR_PHYSIQUE")
            if curObj.graphics.exprs==input_signal then
                num_in=j;
            end
        end
    end
    
    if num_in==[] then
        messagebox('La grandeur physique '+input_signal+' n ''est pas définie')
        err=1
        return
    end

    // On crée un bloc grandeur physique d'entrée en tant qu'objet nombre blocs+1 avec le numéro d'entrée correspondant
    num_in_modified = N_objs + 1;

    old_num_pin  = scs_m.objs(num_in).graphics.pin(1);
    old_num_pout = scs_m.objs(num_in).graphics.pout(1);

    // Crée un nouveau bloc IN_f
    objs(num_in_modified) = IN_f('define');
    objs(num_in_modified).graphics.pout = old_num_pout;
    
    // On remplace le blocs IN_Bode par un GOTO (ce qui vient avant le bloc grandeur physique est alors inutilisé)
    objs(num_in) = GOTO('define');
    objs(num_in).graphics.pin = old_num_pin; //on garde le numero du lien d'entrée de l'ancien bloc
    
    // pour le bloc IN_f
    // On modifie le from-to du lien correspondant
    objs(old_num_pout).from = [num_in_modified 1 0];
    //---------- Traitement de l'entrée -----------

    //---------- Traitement de la sortie ----------
    // On récupère le bloc grandeur physique de sortie
    for j = 1:N_objs
        curObj= objs(j);
        if (typeof(curObj) == "Block" & curObj.gui == "GRANDEUR_PHYSIQUE")
            if curObj.graphics.exprs==output_signal then
                num_out=j;
            end
        end
    end
    if num_out==[] then
        messagebox('La grandeur physique '+output_signal+'n ''est pas définie')
        err=1
        return
    end

    // On crée un bloc OUT_f en tant qu'objet nombre blocs+1 avec le numéro de sortie correspondant
    num_out_modified = N_objs+2;

    old_num_pin  = scs_m.objs(num_out).graphics.pin(1);
    old_num_pout = scs_m.objs(num_out).graphics.pout(1);
    
    objs(num_out_modified) = OUT_f('define') // Crée un nouveau bloc out_f
    objs(num_out_modified).graphics.pin = old_num_pin;

    // On remplace le blocs grandeur physique de sortie par un bloc CONST_m 
    objs(num_out)=CONST_m('define');
    objs(num_out).graphics.pout = old_num_pout;

    //On modifie le from-to du lien correspondant
    objs(old_num_pin).to = [num_out_modified 1 1]

    //---------- Traitement de la sortie ----------


    scs.objs = objs;
endfunction
