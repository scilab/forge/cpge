//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=SCOPE(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'plot' then
        // deprecated
    case 'getinputs' then
        // deprecater
    case 'getoutputs' then
        // deprecated
    case 'getorigin' then
        // deprecated
    case 'set' then
        x=arg1;
        graphics=arg1.graphics;
        exprs=graphics.exprs
        model=arg1.model;

        while %t do
            [ok,nb_output,exprs]=scicos_getvalue('Scope parameters',..
            [gettext('Nombre de courbes')], ..
            list('vec',1), ..
            exprs(1))

            mess=[];
            if ~ok then
                // Cancel
                break;
            end

            if nb_output <= 0 | nb_output>=8
                mess=[mess ;_("Le nombre de courbes doit être compris entre 1 et 7")]
                ok = %f
            end


            if ok then
                in = ones(nb_output,1);
                a = nb_output;
                in2 = ones(a,1);
                [model,graphics,ok]=set_io(model,graphics,list([in in2],ones(a,1)),list(),ones(0,1),[]);
                diagram=scicos_diagram();

                for i=1:nb_output
                    scope=TOWS_c('define')
                    scope.graphics.exprs = ["128";"o"+string(i);"0"]
                    scope.model.ipar=[128;2;24;i];
                    scope.graphics.pein = 5*(i-1)+4;
                    scope.graphics.pin = 5*(i-1)+5;

                    clockc=SampleCLK('define')
                    clockc.graphics.peout=5*(i-1)+5
                    clockc.graphics.exprs=["1" ; "0"]
                    clockc.model.rpar = [1 ; 0]
                    
//                    clockc=CLKFROM('define')
//                    clockc.graphics.peout=5*(i-1)+5
//                    clockc.graphics.exprs = "clock_cpge"
//                    clockc.model.opar(1) = "clock_cpge"
                   
                    

                    input_port=IN_f('define')
                    input_port.graphics.exprs=[string(i);string(1);string(1)]
                    input_port.model.ipar=[i]
                    input_port.graphics.pout=5*(i-1)+4

                    diagram.objs(5*(i-1)+1)=input_port;
                    diagram.objs(5*(i-1)+2)=scope;
                    diagram.objs(5*(i-1)+3)=clockc;
                    diagram.objs(5*(i-1)+4)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1, 1], from=[5*(i-1)+1, 1, 0], to=[5*(i-1)+2, 1, 1])
                    diagram.objs(5*(i-1)+5)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[5, -1], from=[5*(i-1)+3, 1, 0], to=[5*(i-1)+2, 1, 1])
                end

                model.rpar=diagram;
                graphics.exprs(1) = exprs;
                x.model=model;
                x.graphics = graphics;
                break
            else
                message(mess);
            end



        end

        if ok  then

            str_gettext='[';
            labels='';
            list_='list(';
            names_='[';

            for i=1:nb_output
                labels=labels+'label'+string(i)+',';
                str_gettext=str_gettext+'gettext('"Nom de la courbe '+string(i)+''")'
                list_=list_+'''str'',-1';
                if size(graphics.exprs,1)==nb_output+1 then
                    names_=names_+''''+graphics.exprs(1+i)+'''';
                else
                    names_=names_+"''Courbe "+string(i)+"''"
                end
                if i~=nb_output then
                    str_gettext=str_gettext+';';
                    list_=list_+",";
                    names_=names_+";";
                else
                    str_gettext=str_gettext+']';
                    list_=list_+')';
                    names_=names_+"]";
                end
            end

            exec_string='[ok,'+labels+'exprs]=scicos_getvalue(''Paramètres optionnels'','+str_gettext+','+list_+','+names_+')';

            while %t do

                execstr(exec_string);

                if ~ok then
                    break;
                end

                if ok then
                    graphics.exprs= [graphics.exprs(1);exprs];
                    x.model=model;
                    x.graphics = graphics;
                    break
                else
                    message(mess);
                end



            end
        end

    case 'define' then
        nb_output = 1;
        nb_pts=200;
        labels="courbe";

        diagram=scicos_diagram();
        for i=1:nb_output
            scope=TOWS_c('define')
            scope.graphics.exprs = [string(nb_pts);"o"+string(i);"0"]
            scope.model.ipar=[nb_pts;2;24;i];
            scope.graphics.pein = 5*(i-1)+4;
            scope.graphics.pin = 5*(i-1)+5;

            clockc=SampleCLK('define')
            clockc.graphics.peout=5*(i-1)+5
            clockc.graphics.exprs=["0.1" ; "0"]
            clockc.model.rpar = [0.1 ; 0]
            
//            clockc=CLKFROM('define')
//            clockc.graphics.peout=5*(i-1)+5
//            clockc.graphics.exprs = "clock_cpge"
//            clockc.model.opar(1) = "clock_cpge"
                    
            input_port=IN_f('define')
            input_port.graphics.exprs=[string(i);string(1);string(1)]
            input_port.graphics.pout=5*(i-1)+4

            diagram.objs(5*(i-1)+1)=input_port;
            diagram.objs(5*(i-1)+2)=scope;
            diagram.objs(5*(i-1)+3)=clockc;
            diagram.objs(5*(i-1)+4)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1, 1], from=[5*(i-1)+1, 1, 0], to=[5*(i-1)+2, 1, 1])
            diagram.objs(5*(i-1)+5)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[5, -1], from=[5*(i-1)+3, 1, 0], to=[5*(i-1)+2, 1, 1])
        end

        model = scicos_model();
        model.sim='csuper'
        model.in=-1
        model.in2=-2
        model.intyp=-1
        model.blocktype='h'
        model.dep_ut=[%f %f]
        model.rpar=diagram
        x = standard_define([2 2], model, "", [])
        x.gui='SCOPE'
        x.graphics.exprs=[string(nb_output);labels]
    end
endfunction

