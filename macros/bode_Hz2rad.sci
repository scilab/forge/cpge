function []=bode_Hz2rad(h,marges)
//Cette fonction permet de modifier les diagramme de Bode pour un affichage en rad/s et non plus en Hz
//h est un hanlde d'une figure contenant des diagrammes de Bode
//
//

//Pour la courbe de phpase
h.children(1).children.children.data(:,1)=h.children(1).children.children.data(:,1)*2*%pi;
h.children(1).data_bounds(1)=min(h.children(1).children.children.data(:,1));
h.children(1).data_bounds(2)=max(h.children(1).children.children.data(:,1));
h.children(1).x_label.text="Pulsation (rad/s)"

xmin1=min(h.children(1).children.children.data(:,1))
xmax1=max(h.children(1).children.children.data(:,1))
ymax1=max(h.children(1).children.children.data(:,2))
ymin1=min(h.children(1).children.children.data(:,2))
if (marges==1) then
    ymin1=min(ymin1,-180);
end

rect=[xmin1,ymin1,xmax1,ymax1]
replot(rect,h.children(1))

//pour la courbe de gain
h.children(2).children.children.data(:,1)=h.children(2).children.children.data(:,1)*2*%pi;
h.children(2).data_bounds(1)=min(h.children(2).children.children.data(:,1));
h.children(2).data_bounds(2)=max(h.children(2).children.children.data(:,1));
h.children(2).x_label.text="Pulsation (rad/s)"

xmin2=min(h.children(2).children.children.data(:,1))
xmax2=max(h.children(2).children.children.data(:,1))
ymax2=max(h.children(2).children.children.data(:,2))
ymin2=min(h.children(2).children.children.data(:,2));

rect=[xmin2,ymin2,xmax2,ymax2]
replot(rect,h.children(2))

//ged(2,0);
endfunction