//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x, y, typ]=NI600X_C_READ(job, arg1, arg2)
    // Cette fonction correspond à un bloc qui permet de lire la valeur d'une voie de la carte NI à chaque pas de temps 
    x=[];
    y=[];
    typ=[];
    select job
     case 'plot' then
// deprecated
     case 'getinputs' then
// deprecater
     case 'getoutputs' then
// deprecated
     case 'getorigin' then
// deprecated
 case 'set' then
     
      x=arg1;
      graphics=arg1.graphics;
      exprs=graphics.exprs
      model=arg1.model;

// Boite de dialogue pour indiquer les paramètres de l'acquisition
    while %t do
      [ok,devno,channel,acqtype,exprs]=scicos_getvalue("Acquisition parameters",..
                                                ["Device number";"Channel (0,1,2...)";"Acquisition type (0 for RSE, 1 for Diff)"],list('vec',-1,'vec',-1,'vec',-1),exprs);
          mess=[];

          if ~ok then
// Cancel
              break;
          end
            // Verification de l'existence des paramètres à faire varier
            // La verification dans le contexte est faite automatiquement dans le scicos_getvalue

          if ok then
// Everything's ok
              // On a besoin de stocker le nom des variables dans le contexte et plus leur valeur
              // Donc on stocke les paramètres importants
              model.rpar=[devno,channel,acqtype]
              graphics.exprs=exprs;
              x.model=model;
              x.graphics=graphics;
              break;
          else
              message(mess);
          end

end
     case 'define' then
      model=scicos_model();
      model.sim=list("NI600X_C_READ_sim", 5)
      model.blocktype='d';
      model.dep_ut=[%f %f];
      model.out=[1];
      model.evtin=[1]
      model.firing=[0;-1]
      x=standard_define([2 2],model,[],[]);
      x.graphics.in_implicit=[];
      x.graphics.out_implicit=['E'];
      x.graphics.style=["blockWithLabel;verticalLabelPosition=center;displayedLabel= NI Device %s channel %s"]
      x.graphics.exprs=list("1","0","0")
      model.rpar=[1,0,0]
      
    end
endfunction