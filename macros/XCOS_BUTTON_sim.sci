//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function block=XCOS_BUTTON_sim(block,flag)
    global xcos_buttons figure_xcos_buttons 

    select flag
    case -5 // Error

    case 0 // Derivative State Update


    case 1 // Output Update
        //DEBUG("Output update");
        // Necessité de mettre ici la recherche du bloc a partir de son nom car l'update du rep ne fonctionne pas dans xcos_buttons_display
        for i=1:size(xcos_buttons.names,'*')
            if block.opar(1)==xcos_buttons.names(i) then
                block.rpar(3)=i
            end
        end
        rep=block.rpar(3)

        if xcos_buttons.button(rep).value==1 then
            block.outptr(1) =block.rpar(1)
        else
            block.outptr(1) =block.rpar(2)
        end

    case 2 // State Update

    case 3 // OutputEventTiming

    case 4 // Initialization

//        for i=1:size(xcos_buttons.names,'*')
//            if block.opar(1)==xcos_buttons.names(i) then
//                block.rpar(3)=i
//            end
//        end

    case 5 // Ending
        // FIXME: quoi faire a la fin de la simulation

    case 6 // Re-Initialisation

    case 9 // ZeroCrossing

    else // Unknown flag

    end
endfunction


function xcos_button_callback(xcos_buttons,num)
    global xcos_buttons
    path=xcos_buttons.path
    disp(path)
    if xcos_buttons.button(num).value==0 then
        xcos_buttons.img(num).String=path+"xcos_button_on.jpg"
        xcos_buttons.img(num).Value=[0.95 0.95]
        xcos_buttons.button(num).value=1
    else
        xcos_buttons.img(num).String=path+"xcos_button_off.jpg"
        xcos_buttons.img(num).Value=[1 1]
        xcos_buttons.button(num).value=0
    end
    //obj=resume(obj)
endfunction
