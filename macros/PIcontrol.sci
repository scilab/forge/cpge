//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=PIcontrol(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'set' then
        x=arg1;
        graphics=arg1.graphics;
        exprs=graphics.exprs
        model=arg1.model;
        
        newpar=list();
        xx1=model.rpar.objs(1);
        exprs(1)=xx1.graphics.exprs(1);
        P_old=xx1.model.rpar;
        xx2=model.rpar.objs(2);
        exprs(2)=xx2.graphics.exprs(1);
        I_old=xx2.model.rpar;
        y=0
        while %t do
            [ok,P,I,exprs0]=scicos_getvalue('Paramètres du correcteur PI',..
            ["Gain proportionnel";"Gain intégral"],list('vec',1,'vec',1), exprs)
            mess=[];

            if ~ok then break; end

            if ok then
                xx1.graphics.exprs=exprs0(1);
	            xx1.model.rpar=P;
	            xx2.graphics.exprs=exprs0(2);
                xx2.model.rpar=I;
	            arg1.model.rpar.objs(1)=xx1;
	            arg1.model.rpar.objs(2)=xx2;
                graphics.exprs = exprs0;
                x.graphics = graphics;
                break
            else
                message(mess);
            end

        end
        needcompile=0
        if ~(P_old==P & I_old==I ) then
		newpar(size(newpar)+1)=1
		newpar(size(newpar)+1)=2
		needcompile=2
        end
	   x=arg1
	   y=max(y,needcompile)
	   typ=newpar

    
    case 'define' then
        P=1; I=0;  
        diagram=scicos_diagram();

        input_port=IN_f('define');
        input_port.graphics.pout=8;
        output_port=OUT_f('define');
        output_port.graphics.pin=14;     
        gain_p=GAINBLK_f('define')
        gain_p.model.rpar=P;
        gain_p.graphics.exprs=string(P)
        gain_p.graphics.pin=9;
        gain_p.graphics.pout=11;
        gain_i=GAINBLK_f('define')
        gain_i.model.rpar=I;
        gain_i.graphics.pin=10;
        gain_i.graphics.pout=12; 
        gain_i.graphics.exprs=string(I)     
        fct_i=INTEGRAL_m('define');
        fct_i.graphics.pin=12;
        fct_i.graphics.pout=13;   
        sum1=BIGSOM_f('define')
        sum1.model.in=-1*ones(2,1);
        sum1.model.in2=ones(2,1);
        sum1.model.intyp=ones(2,1);
        sum1.model.rpar=ones(2,1);
        sum1.graphics.pin=[11 ;13 ];
        sum1.graphics.pout=14;        
        split1=SPLIT_f('define')
        split1.graphics.pin=8;
        split1.graphics.pout=[9;10];
        split1.model.out= -1*ones(2,1)
        split1.graphics.out_implicit= ["E";"E"]
        diagram.objs(1)=gain_p;
        diagram.objs(2)=gain_i;
        diagram.objs(3)=fct_i;
        diagram.objs(4)=sum1;
        diagram.objs(5)=split1;
        diagram.objs(6)=input_port;
        diagram.objs(7)=output_port;
        
        diagram.objs(8)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[6, 1, 0], to=[5, 1, 1])
        diagram.objs(9)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[5, 1, 0], to=[1, 1, 1])
        diagram.objs(10)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[5, 2, 0], to=[2, 1, 1])
        diagram.objs(11)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[1, 1, 0], to=[4, 1, 1])
        diagram.objs(12)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[2, 1, 0], to=[3, 1, 1])
        diagram.objs(13)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[3, 1, 0], to=[4, 2, 1])
        diagram.objs(14)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[4, 1, 0], to=[7, 1, 1])
        
        
        model=scicos_model();
        model.sim='csuper'
        model.blocktype='h';
        model.dep_ut=[%f %f];
        model.rpar=diagram
        model.out=-1
        model.out2=-2
        model.outtyp=-1
        model.in=-1
        model.in2=-2
        model.intyp=-1

        x=standard_define([2 2],model,[],[]);
        x.graphics.in_implicit=['E'];
        x.graphics.out_implicit=['E'];
        x.graphics.style=["blockWithLabel;verticalLabelPosition=center;displayedLabel=PI"]
        x.graphics.exprs=string([P;I]);
    end
endfunction


