//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010-2010 - DIGITEO -
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function continueSimulation=REP_FREQ_pre_simulate_old(scs_m, needcompile)
    scs=[]

    // On recopie le scs_m
    scs=scs_m;
    // Retrieve all objects
    objs = scs_m.objs;

    nombre_blocs=0;    //Nombre de blocs dans le diagramme
    nombre_liens=0;    //Nombre de lien dans le diagramme
    nombre_rep_freq=0; //Nombre de blocs REP_FREQ dans le diagramme
    nombre_grandeur_phy=0;  //Nombre de blocs IN_Bode dans le diagramme
    num_grandeur_phy=[];    //Numéro des blocs grandeur physique dans le diagramme
    input_signals=[];   //vecteur des grandeurs d'entrée
    output_signals=[];   // vecteur des grandeurs de sortie
    type_diag=[];        // vecteur des types de diagrammes à tracer
    //    nombre_out_bode=0;  //Nombre de blocs OUT_Bode dans le diagramme
    //    num_out_bode=[];    //Numéro des blocs OUT_bode dans le diagramme
    wmin=0;
    wmax=0;
    num_w=0;            // Nombre de fréquences à tracer
    omega=[]             //vecteur des pulsations à tracer dans la réponse fréquentielle
    marges=0;    //marges à ne pas tracer
    presence_delay=%f; //flag indiquant si le schéma contient un bloc TIME_DELAY
    
    //Récupère le nombre de blocs dans le modèle
    for i=1:size(objs)
        if typeof(objs(i))=='Block' then
            nombre_blocs=nombre_blocs+1;
        end
    end
    nombre_liens=size(objs)-nombre_blocs;  //calcul du nombre de liens

    // Récupère les blocs REP_freq et les paramètres associés
    for i=1:nombre_blocs
        if objs(i).gui=="REP_FREQ" then nombre_rep_freq=nombre_rep_freq+1;
            num_rep_freq(nombre_rep_freq)=i;
            input_signals(nombre_rep_freq)=objs(i).model.opar(1);
            output_signals(nombre_rep_freq)=objs(i).model.opar(2);
            type_diag(nombre_rep_freq)=objs(i).model.ipar;
            w_min=objs(i).model.rpar(1);
            w_max=objs(i).model.rpar(2);
            if exists("objs(i).model.rpar(3)") then
                if ~isempty(objs(i).model.rpar(3)) then
                    num_w=objs(i).model.rpar(3);
                end
            end
            marges=objs(i).model.rpar(4);
        end
        
    end

    // s'il y a déja des blocs IN_f et OUT_f, on demande à l'utilisateur de les retirer
    probleme_in_f=%f
    for i=1:nombre_blocs
        if objs(i).gui=="IN_f" | objs(i).gui=="OUT_f" then probleme_in_f=%t;
        end
    end
    if probleme_in_f then disp('Le tracé des réponses fréquentielles est incompatible avec la présence de blocs IN_f et OUT_f')
    end


    //-------------------------- Traitement à proprement parler
    // Pour chacun des bloc REP_FREQ trouvés dans le diagramme
    for i=1:nombre_rep_freq   //i est le bloc rep_freq que l'on traite
        erreur1=%f;

        scs=scs_m; // On repart sur un diagramme propre
        num_in=[] // le numéro du bloc d'entrée dans le diagramme original
        num_out=[] // Le numéro du bloc de sortie dans le diagramme original



        // On récupère le bloc grandeur physique d'entrée
        for j = 1:size(scs.objs)
            curObj= scs.objs(j);
            if (typeof(curObj) == "Block" & curObj.gui == "GRANDEUR_PHYSIQUE")
                if curObj.graphics.exprs==input_signals(i) then
                    num_in=j;
                end
            end
            
            //recherche de la présence d'un bloc retard
            if (typeof(curObj)=='Block'  & curObj.gui=="TIME_DELAY") then
                presence_delay=%t;
                delay_parameter=curObj.model.rpar(1);
                //effacement du bloc TIME_DELAY et remplacement par un gain unitaire pour simplifier
                pin=curObj.graphics.pin;
                pout=curObj.graphics.pout;
                gain=GAINBLK_f('define')
                gain.graphics.pin = pin;
                gain.graphics.pout = pout;
                scs.objs(j)=gain;
            end
            
        end

        //On recalcule le nombre de bloc
        nombre_blocs=0
        for iii=1:size(scs.objs)
            if typeof(scs.objs(iii))=='Block' then
                nombre_blocs=nombre_blocs+1;
            end
        end
        nombre_liens=size(scs.objs)-nombre_blocs;  //calcul du nombre de liens


        // Décale tous les liens d'un cran
        for k=1:nombre_liens
            scs.objs(nombre_blocs+nombre_liens+2-k)=scs.objs(nombre_blocs+nombre_liens+1-k);
        end

        //On décale tout les pin et pout des blocs pour rester en concordances avec les liens
        for l=1:nombre_blocs
            if ~isempty(scs.objs(l).graphics.pin) then scs.objs(l).graphics.pin=scs.objs(l).graphics.pin+1
            end
            if ~isempty(scs.objs(l).graphics.pout) then scs.objs(l).graphics.pout=scs.objs(l).graphics.pout+1
            end
            if ~isempty(scs.objs(l).graphics.pein) then scs.objs(l).graphics.pein=scs.objs(l).graphics.pein+1
            end
            if ~isempty(scs.objs(l).graphics.peout) then scs.objs(l).graphics.peout=scs.objs(l).graphics.peout+1
            end
        end




        // Crée un bloc grandeur physique d'entrée en tant qu'objet nombre blocs+1 avec le numéro d'entrée correspondant

        nombre_blocs=nombre_blocs+1;   // Le nombre de bloc est incrémenté
        scs.objs(nombre_blocs)=IN_f('define') // Crée un nouveau bloc IN_f
        scs.objs(nombre_blocs).graphics.exprs=scs_m.objs(num_in).graphics.exprs  // On récupère le numéro de l'entrée
        //Remplace le blocs IN_Bode par un sommateur
        scs.objs(num_in)=BIGSOM_f('define')


        // Crée un nouveau lien
        nombre_liens=nombre_liens+1
        scs.objs(size(scs.objs)+1)=scicos_link()


        // On relie les nouveaux blocs aux liens existants et au nouveau lien

        // pour le sommateur
        scs.objs(num_in).graphics.orig=scs_m.objs(num_in).graphics.orig
        scs.objs(num_in).graphics.pin= [scs_m.objs(num_in).graphics.pin(1)+1;nombre_liens+nombre_blocs]
        scs.objs(num_in).graphics.pout= scs_m.objs(num_in).graphics.pout+1
        scs.objs(num_in).graphics.sz= [40,40]

        //Pour le bloc IN_f
        scs.objs(nombre_blocs).graphics.orig=[scs_m.objs(num_in).graphics.orig(1)+30,scs_m.objs(num_in).graphics.orig(2)-30]
        scs.objs(nombre_blocs).graphics.sz=[40,40]
        scs.objs(nombre_blocs).graphics.pout=nombre_liens+nombre_blocs

        //Pour le lien qui a été créé
        scs.objs(size(scs.objs)).xx=[scs.objs(nombre_blocs).graphics.orig(1) scs_m.objs(num_in).graphics.orig(1)]
        scs.objs(size(scs.objs)).yy=[scs.objs(nombre_blocs).graphics.orig(2) scs_m.objs(num_in).graphics.orig(2)]
        scs.objs(size(scs.objs)).id='drawlink'
        scs.objs(size(scs.objs)).thick=[0 0]
        scs.objs(size(scs.objs)).ct=[1 1]
        scs.objs(size(scs.objs)).from=[nombre_blocs 1 0]
        scs.objs(size(scs.objs)).to=[num_in 2 1]



        //-------------------------------------
        // Traitement de la sortie
        objs=scs.objs
        //Récupère le bloc grandeur physique de sortie
        for j = 1:size(scs.objs)
            curObj= scs.objs(j);
            if (typeof(curObj) == "Block" & curObj.gui == "GRANDEUR_PHYSIQUE")
                if curObj.graphics.exprs==output_signals(i) then
                    num_out=j;
                end
            end
        end


        // Décale tout les liens d'un cran
        for k=1:nombre_liens
            scs.objs(nombre_blocs+nombre_liens+2-k)=scs.objs(nombre_blocs+nombre_liens+1-k);
        end

        //On décale tout les pin et pout des blocs pour rester en concordances avec les liens
        for l=1:nombre_blocs
            if ~isempty(scs.objs(l).graphics.pin) then scs.objs(l).graphics.pin=scs.objs(l).graphics.pin+1
            end
            if ~isempty(scs.objs(l).graphics.pout) then scs.objs(l).graphics.pout=scs.objs(l).graphics.pout+1
            end
            if ~isempty(scs.objs(l).graphics.pein) then scs.objs(l).graphics.pein=scs.objs(l).graphics.pein+1
            end
            if ~isempty(scs.objs(l).graphics.peout) then scs.objs(l).graphics.peout=scs.objs(l).graphics.peout+1
            end
        end

        // Crée un bloc OUT_f en tant qu'objet nombre blocs+1 avec le numéro de sortie correspondant

        nombre_blocs=nombre_blocs+1;   // Le nombre de bloc est incrémenté
        scs.objs(nombre_blocs)=OUT_f('define') // Crée un nouveau bloc out_f
        scs.objs(nombre_blocs).graphics.exprs=scs_m.objs(num_out).graphics.exprs  // On récupère le numéro de l'entrée

        //Remplace le blocs grandeur physique de sortie par un embranchement (split_f)
        scs.objs(num_out)=SPLIT_f('define')


        // Crée un nouveau lien
        nombre_liens=nombre_liens+1
        scs.objs(size(scs.objs)+1)=scicos_link()


        // On relie les nouveaux blocs aux liens existants et au nouveau lien

        // pour le split_f
        scs.objs(num_out).graphics.orig=scs_m.objs(num_out).graphics.orig
        scs.objs(num_out).graphics.pin= [scs_m.objs(num_out).graphics.pin(1)]
        scs.objs(num_out).graphics.pout= [scs_m.objs(num_out).graphics.pout;nombre_liens+nombre_blocs]


        //Pour le bloc OUT_f
        scs.objs(nombre_blocs).graphics.orig=[scs_m.objs(num_out).graphics.orig(1)+10,scs_m.objs(num_out).graphics.orig(2)-30]
        scs.objs(nombre_blocs).graphics.sz=[40,40]
        scs.objs(nombre_blocs).graphics.pin=nombre_liens+nombre_blocs

        //Pour le lien qui a été créé
        scs.objs(size(scs.objs)).xx=[scs.objs(nombre_blocs).graphics.orig(1) scs_m.objs(num_out).graphics.orig(1)]
        scs.objs(size(scs.objs)).yy=[scs.objs(nombre_blocs).graphics.orig(2) scs_m.objs(num_out).graphics.orig(2)]
        scs.objs(size(scs.objs)).id='drawlink'
        scs.objs(size(scs.objs)).thick=[0 0]
        scs.objs(size(scs.objs)).ct=[1 1]
        scs.objs(size(scs.objs)).from=[num_out 2 0]
        scs.objs(size(scs.objs)).to=[nombre_blocs 1 1]

        // On linearise le modele
        try
            sys=lincos(scs)
        catch
            message(["Scilab n a pas réussi à linéariser le système" "Vérifier que tous les blocs sont linéaires et causaux"])
            erreur1=%t
        end

        if ~erreur1
            tf=ss2tf(sys)
            // On indique la fonction de transfert dans la console :
            disp(["Scilab a réussi à extraire une fonction de transfert du schéma bloc : ("+sci2exp(tf(2))+")/("+sci2exp(tf(3))+")"])

            // On construit le vecteur des omega
            if num_w>0 then
                omega=logspace(log10(w_min/(2*%pi)),log10(w_max/(2*%pi)),num_w);
            end


            // On choisit le diagramme à tracer

            h=figure();

            select type_diag(i)
            case 1
                if num_w>0 then
                    try
                        h1=sys;
                        if presence_delay==%t then
                            h1=iodelay(tf,delay_parameter);
                        end
                        bode(h1,omega);
                    catch
                        disp(["Scilab a réussi à extraire une fonction de transfert du schéma bloc mais n''est pas parvenu à tracer la réponse fréquentielle.","La fonction de transfert obtenue est : ("+sci2exp(tf(2))+")/("+sci2exp(tf(3))+")"])

                    end
                else
                    try
                        h1=sys;
                        if presence_delay==%t then
                            h1=iodelay(tf,delay_parameter);
                        end
                        bode(h1,w_min/(2*%pi),w_max/(2*%pi));
                        //bode(sys,w_min/(2*%pi),w_max/(2*%pi));
                    catch
                        disp(["Scilab a réussi à extraire une fonction de transfert du schéma bloc mais n''est pas parvenu à tracer la réponse fréquentielle.","La fonction de transfert obtenue est : ("+sci2exp(tf(2))+")/("+sci2exp(tf(3))+")"])

                    end
                end
                bode_Hz2rad_2(h,marges);
                if marges==1 then
                    freq_analysis(sys,'bode');
                end

            case 2

                if num_w>0 then
                    try
                        black(sys,omega);
                        if(h.children.data_bounds(1)>-180) 
                            h.children.data_bounds(1)=-180;
                        end
                        nicholschart(colors=color('light gray')*[1 1])
                    catch
                        disp(["Scilab a réussi à extraire une fonction de transfert du schéma bloc mais n''est pas parvenu à tracer la réponse fréquentielle.","La fonction de transfert obtenue est : ("+sci2exp(tf(2))+")/("+sci2exp(tf(3))+")"])

                    end
                else
                    try
                        black(sys,w_min/(2*%pi),w_max/(2*%pi));
                        if(h.children.data_bounds(1)>-180) 
                            h.children.data_bounds(1)=-180;
                        end
                        
                        nicholschart(colors=color('light gray')*[1 1])
                    catch
                        disp(["Scilab a réussi à extraire une fonction de transfert du schéma bloc mais n''est pas parvenu à tracer la réponse fréquentielle.","La fonction de transfert obtenue est : ("+sci2exp(tf(2))+")/("+sci2exp(tf(3))+")"])
                    end
                end
                if marges==1 then
                    freq_analysis(sys,'black');
                end

            case 3
                if num_w>0 then
                    try
                        nyquist(sys,omega);
                    catch
                        disp(["Scilab a réussi à extraire une fonction de transfert du schéma bloc mais n''est pas parvenu à tracer la réponse fréquentielle.","La fonction de transfert obtenue est : ("+sci2exp(tf(2))+")/("+sci2exp(tf(3))+")"])
                    end
                else
                    try
                        nyquist(sys,w_min/(2*%pi),w_max/(2*%pi));
                    catch
                        disp(["Scilab a réussi à extraire une fonction de transfert du schéma bloc mais n''est pas parvenu à tracer la réponse fréquentielle.","La fonction de transfert obtenue est : ("+sci2exp(tf(2))+")/("+sci2exp(tf(3))+")"])
                    end
                end
            end

            set(h,"background",8)

        end
    end

    // Force Simulation to start after pre_simulation process.
    continueSimulation = %t
endfunction