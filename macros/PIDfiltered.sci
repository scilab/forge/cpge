//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=PIDfiltered(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'plot' then
        // deprecated
    case 'getinputs' then
        // deprecater
    case 'getoutputs' then
        // deprecated
    case 'getorigin' then
        // deprecated
    case 'set' then
        x=arg1;
        graphics=arg1.graphics;
        exprs=graphics.exprs
        model=arg1.model;
        x0=model.rpar.objs(1).model.state
        rpar=model.rpar.objs(1).model.rpar
        ns=prod(size(x0))
        while %t do
            [ok,P,I,D,tauc,exprs]=scicos_getvalue('Set PID parameters',..
            [gettext('Gain proportionnel');gettext('Gain intégral');gettext('Gain dérivé');gettext('Constante de temps pour le filtre sur la dérivée')],list('vec',1,'vec',1,'vec',1,'vec',1), exprs)
            mess=[];

            if ~ok then break; end

            if ok then
                
                s=poly(0,'s')
                ft=P+I/s+D*s/(1+s*tauc)
                H=tf2ss(ft)
                [A,B,C,D]=H(2:5);
                
                [ns1,ns1]=size(A)
               
                rpar=[matrix(A,ns1*ns1,1);                
                matrix(B,ns1,1);
                matrix(C,ns1,1);
                D]
                
                if norm(D,1)<>0 then
                    mmm=[%t %t];
                else
                    mmm=[%f %t];
                end

                if or(model.dep_ut<>mmm) then
                    model.dep_ut=mmm,
                end

                if ns1<=ns then
                    x0=x0(1:ns1)
                else
                    x0(ns1,1)=0
                end
                model.rpar.objs(1).graphics.exprs=[pol2str(ft.num);pol2str(ft.den)]
                model.rpar.objs(1).model.state=x0
                model.rpar.objs(1).model.rpar=rpar
                graphics.exprs=exprs;
                x.graphics=graphics;
                x.model=model
                break
            else
                message(mess);
            end

        end
    case 'define' then
        P=1; I=1; D=0; tauc=1e-6;   
        diagram=scicos_diagram();

        input_port=IN_f('define');
        input_port.graphics.pout=4;
        output_port=OUT_f('define');
        output_port.graphics.pin=5;     
        fct=CLR('define');
        fct.model.state=0
        fct.model.rpar=[0;1;1;1]
        fct.model.blocktype="c"
        fct.model.dep_ut=[%f %t]
        fct.graphics.pin=4;
        fct.graphics.pout=5;  
        fct.graphics.exprs=["1+s";"s"]  
        diagram.objs(1)=fct;
        diagram.objs(2)=input_port;
        diagram.objs(3)=output_port;
        diagram.objs(4)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[2, 1, 0], to=[1, 1, 1])
        diagram.objs(5)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[1, 1, 0], to=[3, 1, 1])
        
        
        model=scicos_model();
        model.sim='csuper'
        model.blocktype='h';
        model.dep_ut=[%f %f];
        model.rpar=diagram
        model.out=-1
        model.out2=-2
        model.outtyp=-1
        model.in=-1
        model.in2=-2
        model.intyp=-1

        x=standard_define([2 2],model,[],[]);
        x.graphics.in_implicit=['E'];
        x.graphics.out_implicit=['E'];
        x.graphics.style=["blockWithLabel;verticalLabelPosition=center;displayedLabel=PID"]
        x.graphics.exprs=string([P;I;D;tauc]);
    end
endfunction


