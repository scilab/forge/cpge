//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010-2012 - Scilab Enterprises - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function continueSimulation=Param_Var_pre_simulate(scs_m, needcompile)

    //On crée un nouveau fichier scs pour ne pas écraser le premier
    scs=scs_m
    objs = scs_m.objs;

    param1="";
    param1val=[];
    param2="";
    param2val=[];
    param3="";
    param3val=[];

    todemux = %f;

    continueSimulation = %f;

    // On récupere le numéro du bloc PARAM_VAR puis les caractéristiques correspondantes
    for i = 1:size(objs)
        curObj= objs(i);
        if (typeof(curObj) == "Block" & curObj.gui == "PARAM_VAR")
            if(length(curObj.model.opar)==0) then
                message(['Double-cliquer sur le bloc PARAM. variation pour mettre à jour les paramètres'])
                return;
            end
            if length(curObj.model.opar)>0 then
                param1=curObj.model.opar(1)
                param1val=curObj.model.opar(2)
            end
            if length(curObj.model.opar)>2 then
                param2=curObj.model.opar(3)
                param2val=curObj.model.opar(4)
            end
            if length(curObj.model.opar)>4 then
                param3=curObj.model.opar(5)
                param3val=curObj.model.opar(6)
            end
        elseif (typeof(curObj) == "Block" & curObj.gui == "MUX")
            todemux = %t;
        end
    end

    nombre_scope=0 //Nombre de scope trouvé
    nom_scope=list();
    //On va modifier le scs pour remplacer les auto scope et les scopes du diagramme par des bloc to_workspace
    for i =  1:size(objs)
        curObj= objs(i);
        if (typeof(curObj) == "Block" & (curObj.gui == "AUTO_SCOPE"|curObj.gui == "CSCOPE"))   //On vérifie qu'on a trouvé un bloc autoscope
            //if (curObj.model.rpar.objs(1).gui=="CSCOPE"|curObj.model.rpar.objs(2).gui=="CSCOPE") then
            scopetype=curObj.gui;
            nombre_scope=nombre_scope+1;
            select scopetype
            case "AUTO_SCOPE"
                // On récupere le nom du scope
                nom_scope(nombre_scope)=objs(i).model.rpar.objs(2).graphics.exprs(10);

                // Crée un bloc to sample_CLK en tant qu'objet nombre blocs+1 avec le numéro d'entrée correspondant
                // Le plus simple est de recopier le bloc sample_CLK du scope auto
                num_autoscope_sampleclk = size(objs) + 1;
                
                objs(num_autoscope_sampleclk) = objs(i).model.rpar.objs(3) // Crée un nouveau bloc clock identique à celui qui était dans le scope auto

                // Si on traite le 1er scope on demande les paramètres de la simulation
                //            if  nombre_scope==1
                //                [ok,num_p,temps]=getvalue('Paramètre de la simulation',["Nombre de points","Durée de simulation"],list('vec','1','vec',1),['200','20']);
                //            end

                temps=evstr(objs(i).graphics.exprs(2));
                num_p=evstr(objs(i).graphics.exprs(1));

                objs(num_autoscope_sampleclk).model.rpar=[temps/num_p,0]
                objs(num_autoscope_sampleclk).graphics.exprs=list(string(temps/num_p),"0")


                //Remplace le blocs scope auto par un bloc to_workspace
                // on récupere d'abord le numero du lien d'éntree
                graphics = objs(i).graphics
                objs(i)=TOWS_c('define')
                objs(i).graphics.orig=graphics.orig
                objs(i).graphics.sz=graphics.sz

                objs(i).graphics.exprs=[string(num_p);"A"+string(nombre_scope);"0"]
                objs(i).graphics.pin=graphics.pin

                objs(i).model.ipar=[num_p;1;-10];

                //On crée le lien supplémentaire nécessaire
                num_autoscope_sampleclk_link = size(objs) + 2
                objs(num_autoscope_sampleclk_link)=scicos_link()
                objs(num_autoscope_sampleclk_link).xx=[objs(i).graphics.orig(1) objs(num_autoscope_sampleclk).graphics.orig(1)]
                objs(num_autoscope_sampleclk_link).yy=[objs(i).graphics.orig(2) objs(num_autoscope_sampleclk).graphics.orig(2)]
                objs(num_autoscope_sampleclk_link).id='drawlink'
                objs(num_autoscope_sampleclk_link).thick=[0 0]
                objs(num_autoscope_sampleclk_link).ct=[5 -1]
                objs(num_autoscope_sampleclk_link).from=[num_autoscope_sampleclk 1 0]
                objs(num_autoscope_sampleclk_link).to=[i 1 1]

                //On relie la clock au nouveau lien
                objs(num_autoscope_sampleclk).graphics.peout = num_autoscope_sampleclk_link
                //Idem pour l'entre explicite to To_wks
                objs(i).graphics.pein = num_autoscope_sampleclk_link

            case "CSCOPE"
                // On récupere le nom du scope
                nom_scope(nombre_scope) = objs(i).graphics.exprs(10);

                // Si on traite le 1er scope on demande les paramètres de la simulation
                if  nombre_scope==1
                    [ok,num_p,temps]=getvalue('Paramètre de la simulation',["Nombre de points","Durée de simulation"],list('vec','1','vec',1),['200','20']);
                end

                //On recupere l'horloge qui est accroché au scope
                lien_scope = objs(i).graphics.pein
                num_clock = objs(lien_scope).from(1)
                objs(num_clock).model.rpar.objs(2).model.rpar=[temps/num_p,0]
                objs(num_clock).model.rpar.objs(2).graphics.exprs=list(string(temps/num_p),"0")

                //Remplace le blocs scope auto par un bloc to_workspace
                // on récupere d'abord le numero des lien d'éntree
                graphics = objs(i).graphics
                objs(i)=TOWS_c('define')
                objs(i).graphics.orig=graphics.orig
                objs(i).graphics.sz=graphics.sz
                
                objs(i).graphics.exprs=[string(num_p);"A"+string(nombre_scope);"0"]
                objs(i).graphics.pin=graphics.pin
                objs(i).graphics.pein=graphics.pein
                objs(i).model.ipar=[num_p;1;-10];
            end
        end
    end

    scs.props.tf=temps;
    scs.objs = objs;

    num_param=0;
    if ~isempty(param1) then num_param=num_param+1;
    end
    if ~isempty(param2) then num_param=num_param+1;
    end
    if ~isempty(param3) then num_param=num_param+1;
    end


    Nsimu=0  //Numero de la simu
    x=0
    D=[]
    winId=waitbar('Simulation in progress')
    cont=[]
    z=[]


    for z=1:nombre_scope
        handle_fig(z)=figure();
    end
    legend_c=[]

    drawlater()
    //c_color=['m','b','r','k','g','c']
    c_color=[[0.75,0.75,0];[0.25,0.25,0.25];[0,0,1];[0,0.5,0];[1,0,0];[0,0.75,0.75];[0.75,0,0.75]];

    select num_param
    case 1 //1 parametre

        for i=1:length(param1val)
            x=(i-1)/length(param1val)
            waitbar(x,winId)
            // Version initiale
            execstr("cont"+"."+param1+"="+string(param1val(i)))
            scicos_simulate(scs,cont)

            //Version plus évoluée
            //    context=scs.props.context;
            //    context($+1)=string(param1)+"="+string(param1val(i))
            //    [%scicos_context, ierr] = script2var(context,struct())
            //    %cpr=list();
            //
            //    [scs2, %cpr, needcompile, ok] = do_eval(scs, %cpr,%scicos_context);
            //    scicos_simulate(scs2)
            // fin version plus evoluee

            Nsimu=Nsimu+1;
            legend_c(i)=string(param1)+'='+string(param1val(i))
            // On enregistre chacune des variables dans une matrice D(i,j) : la variable i de la simu j est stockée dans D(i,j)

            for l=1:nombre_scope
                D(l,Nsimu)=evstr("A"+string(l))
                scf(handle_fig(l))
                plot(D(l,Nsimu).time,D(l,Nsimu).values,'color',[c_color(modulo(Nsimu,6)+1,1),c_color(modulo(Nsimu,6)+1,2),c_color(modulo(Nsimu,6)+1,3)],'thickness',2)
                set(handle_fig(l),"background",8)
                xgrid
                // A la dernière simu on affiche la légende et le nom du scope sauf si on a tracé plus d'une courbe à chaque fois
                if i==length(param1val)
                    if size(D(1,1),2)==1 then legend(legend_c); end
                    title(nom_scope(l));
                    if (todemux==%t) then handle_fig(l).tag="todemux";
                    else handle_fig(l).tag="nodemux"; end
                end
            end
        end
        delete(winId);

    case 2 //2 parametres

        for i=1:length(param1val)
            execstr("cont"+"."+param1+"="+string(param1val(i)));
            for j=1:length(param2val)
                x=((i-1)*length(param2val)+(j-1))/(length(param1val)*length(param2val));
                waitbar(x,winId);
                execstr("cont"+"."+param2+"="+string(param2val(j)));
                Nsimu=Nsimu+1;
                scicos_simulate(scs,cont);
                legend_c(Nsimu)=string(param1)+'='+string(param1val(i))+', '+string(param2)+'='+string(param2val(j));
                for l=1:nombre_scope
                    D(l,Nsimu)=evstr("A"+string(l));
                    scf(handle_fig(l));
                    plot(D(l,Nsimu).time,D(l,Nsimu).values,'color',[c_color(modulo(Nsimu,6)+1,1),c_color(modulo(Nsimu,6)+1,2),c_color(modulo(Nsimu,6)+1,3)],'thickness',2);
                    set(handle_fig(l),"background",8);
                    xgrid;
                    if i==length(param1val) & j==length(param2val)
                        legend(legend_c);
                        title(nom_scope(l));
                        if (todemux==%t) then handle_fig(l).tag="todemux";
                        else handle_fig(l).tag="nodemux"; end
                    end
                end
            end
        end
        delete(winId);

    case 3 // trois parametres variables
        for i=1:length(param1val)
            execstr("cont"+"."+param1+"="+string(param1val(i)));
            for j=1:length(param2val)
                execstr("cont"+"."+param2+"="+string(param2val(j)));
                for k=1:length(param3val)
                    x=((i-1)*length(param2val)*length(param3val)+(j-1)*length(param3val)+(k-1))/(length(param1val)*length(param2val)*length(param3val));
                    waitbar(x,winId);
                    execstr("cont"+"."+param3+"="+string(param3val(k)));
                    Nsimu=Nsimu+1
                    scicos_simulate(scs,cont);
                    legend_c(Nsimu)=string(param1)+'='+string(param1val(i))+', '+string(param2)+'='+string(param2val(j))+', '+string(param3)+'='+string(param3val(k));
                    for l=1:nombre_scope
                        D(l,Nsimu)=evstr("A"+string(l));
                        scf(handle_fig(l));
                        plot(D(l,Nsimu).time,D(l,Nsimu).values,'color',[c_color(modulo(Nsimu,6)+1,1),c_color(modulo(Nsimu,6)+1,2),c_color(modulo(Nsimu,6)+1,3)],'thickness',2);
                        set(handle_fig(l),"background",8);
                        xgrid;
                        if i==length(param1val) & j==length(param2val) & k==length(param3val)
                            legend(legend_c);
                            title(nom_scope(l));
                            if (todemux==%t) then handle_fig(l).tag="todemux";
                            else handle_fig(l).tag="nodemux"; end
                        end
                    end
                end
            end
        end
        delete(winId);
    end
    nicescope();
    drawnow()
endfunction
