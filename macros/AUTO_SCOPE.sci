//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=AUTO_SCOPE(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
     case 'plot' then
// deprecated
     case 'getinputs' then
// deprecater
     case 'getoutputs' then
// deprecated
     case 'getorigin' then
// deprecated
     case 'set' then
      x=arg1;
      graphics=arg1.graphics;
      exprs=graphics.exprs
      model=arg1.model;

      while %t do
          [ok,num_points,tf,name,exprs]=scicos_getvalue('Scope parameters',..
                                                        [gettext('Number of points');gettext('End simulation time'); gettext('Scope Name')+' (optional)'], ..
                                                        list('vec',1,'vec',1,'str','-1'), ..
                                                        exprs)
          mess=[];

          if ~ok then
// Cancel
              break;
          end

          if num_points <= 0
              mess=[mess ;_("Number of points must be positive.")]
              ok = %f
          end


          if ok then
// Everything's ok
              model.rpar.objs(3).graphics.exprs=[string(tf)+"/"+string(num_points) ; "0"]
              model.rpar.objs(3).model.rpar=[tf/num_points ; 0]
              model.rpar.objs(2).graphics.exprs(7) = sci2exp(tf);
              model.rpar.objs(2).graphics.exprs(10) = name
              model.rpar.objs(2).model.rpar(4) = tf
              model.rpar.objs(4).graphics.exprs = string(tf);
              model.rpar.objs(4).model.firing = tf;
              graphics.exprs = exprs;
              x.model=model;
              x.graphics = graphics;
              break
          else
              message(mess);
          end

      end
     case 'define' then
      tf = 100;
      num_points = 200;

      scope=CSCOPE('define')
      scope.graphics.pin = 5
      scope.graphics.pein = 6
      scope.model.rpar(4) = tf
      scope.graphics.exprs(7) = string(tf);

      clockc=SampleCLK('define')
      clockc.graphics.peout=6
      clockc.graphics.exprs=[string(tf)+"/"+string(num_points) ; "0"]
      clockc.model.rpar = [tf/num_points ; 0]

      input_port=IN_f('define')
      input_port.graphics.exprs=["1"]
      input_port.model.ipar=[1]
      input_port.graphics.pout=5

      endBlock=END_c('define');
      endBlock.graphics.exprs = string(tf);
      endBlock.model.firing = tf;
      endBlock.graphics.pein=7;
      endBlock.graphics.peout=7;


      diagram=scicos_diagram();
      diagram.objs(1)=input_port;
      diagram.objs(2)=scope;
      diagram.objs(3)=clockc;
      diagram.objs(4)=endBlock;
      diagram.objs(5)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1, 1], from=[1, 1, 0], to=[2, 1, 1])
      diagram.objs(6)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[5, -1], from=[3, 1, 0], to=[2, 1, 1])
      diagram.objs(7)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[5, -1], from=[4, 1, 0], to=[4, 1, 1])


      model = scicos_model();
      model.sim='csuper'
      model.in=-1
      model.in2=-2
      model.intyp=-1
      model.blocktype='h'
      model.dep_ut=[%f %f]
      model.rpar=diagram
      x = standard_define([2 2], model, "", [])
      x.gui='AUTO_SCOPE'
      x.graphics.exprs=[string(num_points) ; string(tf) ; ""]
    end
endfunction

