//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
function PIC_end_of_simul()
//    global inc
//    inc=x_dialog("Entrer la valeur de inc :" , "20")
//    disp(inc)
    global port_TCL connected;
    closeserial(port_TCL);
    connected=0; 
    //clear port_TCL;   

endfunction
