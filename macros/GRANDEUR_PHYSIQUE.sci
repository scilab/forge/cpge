//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=GRANDEUR_PHYSIQUE(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
     case 'plot' then
// deprecated
     case 'getinputs' then
// deprecater
     case 'getoutputs' then
// deprecated
     case 'getorigin' then
// deprecated
     case 'set' then
      x=arg1;
      graphics=arg1.graphics;
      exprs=graphics.exprs
      model=arg1.model;
      while %t do
          [ok,Name,exprs]=scicos_getvalue('Donner un nom à la grandeur physique',..
                                                ['Nom '], ..
                                                list('str',1),exprs)
          mess=[];

          if ~ok then
// Cancel
              break;
          end


          if ok then
// Everything's ok
              model.opar=list(Name);
              graphics.exprs=exprs;
              x.model=model;
              x.graphics=graphics;
              break;
          else
              message(mess);
          end

      end
     case 'define' then
      model=scicos_model();
      k=1;
      model.sim=list("GRANDEUR_PHYSIQUE_sim", 5)
      model.blocktype='c';
      model.dep_ut=[%t %f];
      model.in=[1];
      model.intyp=[1];
      model.out=[1];
      model.outtyp=[1];
      model.opar=list();
      x=standard_define([1 1],model,[],[]);
      x.graphics.in_implicit=['E'];
      x.graphics.out_implicit=['E'];
      x.graphics.style=["blockWithLabel;verticalLabelPosition=center;displayedLabel= %s;fillColor=#FF3333;"] //fillcolor=#F6FFFF;strokeColor=#FFFFFF
      x.graphics.exprs="Nom";
    end
endfunction
