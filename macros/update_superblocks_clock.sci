//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

//Mise a jour de l'horloge se trouvant dans les blocs READ et WRITE_CSV
function scs_m=update_superblocks_clock(scs_m,dt)
    for i = 1:size(scs_m.objs)
        curObj= scs_m.objs(i);
        if (typeof(curObj) == "Block" & (curObj.gui == "READ_CSV_E" | curObj.gui == "WRITE_CSV_E" ))
                for j=1:size(scs_m.objs(i).model.rpar.objs)
                    if (typeof(scs_m.objs(i).model.rpar.objs(j)) == "Block" & scs_m.objs(i).model.rpar.objs(j).gui == "SampleCLK") then
                        scs_m.objs(i).model.rpar.objs(j).model.rpar =[dt;0]
                        scs_m.objs(i).model.rpar.objs(j).graphics.exprs=[string(dt);"0"]
                    end
                end
        end
        if (typeof(curObj) == "Block" & (curObj.gui == "READ_CSV_E"))
            csv_path=curObj.graphics.exprs(1)
            if isfile(csv_path)==%F then
                messagebox("Vous devez sélectionner dans le navigateur de fichiers le répertoire contenant votre fichier texte pour qu''il soit reconnu", "READ_CSV_E", "error")
                return
            end
         end
    end
endfunction
