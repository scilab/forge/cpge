
function init_NI(block_NI600X)
      global taskAI0 bufferSize;
    try
        nodevice=block_NI600X.model.rpar(1);
        diff_rse=block_NI600X.model.rpar(2)
        chan=block_NI600X.model.rpar(3)
        periode=10
        
        [taskAI0,err,pointsToRead,rate,bufferSize] = initGetAI(nodevice,chan,diff_rse,periode);
        disp(err)
//        device="Dev"+string(nodevice)
//        channel = "Dev"+string(nodevice)+"/ai"+string(chan);
//        disp(channel)
//        maxVal = 10.0;
//        minVal = -10.0;
//        rate=10000;
//        source = "OnboardClock";
//        timeout = 0;
//        [err] = DAQ_ResetDevice(device);
//        [taskAI,err] = DAQ_CreateTask(""); DAQ_ErrChk(taskAI,err);
//        if diff_rse==0 then 
//            [err] = DAQ_CreateAIVoltageChan(taskAI, channel, DAQ("Val_RSE"),minVal, maxVal);
//        else
//            [err] = DAQ_CreateAIVoltageChan(taskAI, channel, DAQ("Val_Diff"),minVal, maxVal);     
//        end
//        disp(err)
//        DAQ_ErrChk(taskAI,err);
//// Val_FiniteSamps pour N echantillons et Val_ContSamps pour mode continu
//[err] = DAQ_CfgSampClkTiming(taskAI, source, rate,...
//        DAQ("Val_Rising"), DAQ("Val_FiniteSamps"),2);
//        DAQ_ErrChk(taskAI,err);
//     
//[err] = DAQ_StartTask(taskAI); DAQ_ErrChk(taskAI,err);
//disp(err)
    
    catch
        DAQ_StopTask(taskAI0);
        DAQ_ClearTask(taskAI0);        
        messagebox("Numero de device inaccessible")
    end

endfunction


function [task,err,pointsToRead,rate,bufferSize] = initGetAI(nodevice,chan,diff_rse,periode)
    // Voies
    taskName= ""
    device  = "Dev"+string(nodevice);
    chan    = device+"/ai"+string(chan);
    nbchan  = 1;
    minVal  = -10.0;
    maxVal  = +10.0;
    // Cadence
    source         = "OnboardClock";
    rate           = 10000;
    samplesPerChan = rate*periode/1000; 
    bufferSize   = samplesPerChan * nbchan * 1;
    pointsToRead = 1;
    [err] = DAQ_ResetDevice(device);
    [task,err] = DAQ_CreateTask(taskName); DAQ_ErrChk(task,err);
    if diff_rse==0 then
        [err] = DAQ_CreateAIVoltageChan(task, chan, DAQ("Val_RSE"),...
            minVal, maxVal);
    else
        [err] = DAQ_CreateAIVoltageChan(task, chan, DAQ("Val_Diff"),...
            minVal, maxVal);   
    end
     DAQ_ErrChk(task,err);
    [err] = DAQ_CfgSampClkTiming(task, source, 100,...
            DAQ("Val_Rising"), DAQ("Val_ContSamps"),5);
            DAQ_ErrChk(task,err);

                        
    [err] = DAQ_StartTask(task); 
    DAQ_ErrChk(task,err);
    disp('ok')
//    [err] = DAQ_CfgSampClkTiming(task, source, rate,...
//            DAQ("Val_Rising"), DAQ("Val_FiniteSamps"),samplesPerChan);
//            DAQ_ErrChk(task,err);
//    [err] = DAQ_StartTask(task); DAQ_ErrChk(task,err);
//  NE PAS DEMMARRER LA TACHE D'ACQUISITION CONTINUE DANS L'INIT MAIS JUSTE
//  AVANT LA BOUCLE pour synchroniser au mieux.
endfunction

function collectData=test_acq()
    periode=10
    chan=0
    nodevice=2
    [taskAI0,err,pointsToRead,rate,bufferSize] = initGetAI(nodevice,chan,0,periode);
    collectData=[]; tempoData=[];
   // Démarrer l'acquisition continue juste avant la boucle (synchroniser)    
//                [err] = DAQ_StartTask(taskAI0); 
//    DAQ_ErrChk(taskAI0,err);
    
    t=0
    while(t<2)
//        [data,pointsRead,err] = DAQ_ReadAnalogF64(taskAI0,1,1,...
//                                DAQ("Val_GroupByChannel"),bufferSize);
//                                DAQ_ErrChk(taskAI0, err);
        [data,pointsRead,err] = DAQ_ReadAnalogF64(taskAI0,-1,periode,...
                        DAQ("Val_GroupByChannel"),1);
                        DAQ_ErrChk(taskAI0, err); 

        collectData($+1)=data
        sleep(periode)
        t=t+periode/1000;  
        disp(t) 
                                disp(pointsRead)
                        disp(data)
    end
    
    
    // Init cadencement de boucle 
//     realtimeinit(periode/1000);  // définit la période de boucle en s
//     realtime(0);                 // initialise le compteur temporel
//     // Itération---------------------------------------
//
//
//    for indice = 1:50/periode // pour une durée de simulation d'environ 50ms
//        // Lire la tension AI0 (tous les échantillons présents dans le buffer):  
//        [data,pointsRead,err] = DAQ_ReadAnalogF64(taskAI0,-1,10,...
//                                DAQ("Val_GroupByChannel"),bufferSize);
//                                DAQ_ErrChk(taskAI0, err);                     
//        // Collecter et traiter les signaux lus
//        if pointsRead > 0 then
//            dataCut = data(1,1:pointsRead); 
//            collectData = [collectData,dataCut];
//            tempoData   = [tempoData,dataCut*0+modulo(indice,2)*5]; //témoin temporel
//            //disp(size(tempoData),size(collectData),size(dataCut),size(data),pointsRead);
//        end;    
//        realtime(indice); // attend si besoin la date indice*periode/1000
//     end;
     DAQ_StopTask(taskAI0);
    DAQ_ClearTask(taskAI0);
    
endfunction


