//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=MAXPID(job,arg1,arg2)
  x=[];y=[];typ=[];
  select job
   case 'plot' then
    // deprecated
   case 'getinputs' then
    // deprecater
   case 'getoutputs' then
    // deprecated
   case 'getorigin' then
    // deprecated
   case 'set' then
    x=arg1;
   case 'define' then
    model=scicos_model();
    k=1;
    model.sim=list("MAXPID_sim", 5)
    model.blocktype='d';
    model.dep_ut=[%t %f];
    model.in=[1];
    model.intyp=[1];
    model.out=[];
    model.evtin=[1]
    x=standard_define([2 2],model,[],[]);
    x.graphics.in_implicit=['E'];
    x.graphics.out_implicit=[];
  end
endfunction
