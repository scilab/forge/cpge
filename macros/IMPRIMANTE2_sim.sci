//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function block=IMPRIMANTE2_sim(block,flag)
    global port_TCL;
function DEBUG(message)
    disp("[DEBUG time = "+string(scicos_time())+"] {"+block.label+"} ARDUINO_DCMOTOR Simulation: "+message);
endfunction
    select flag
     case -5 // Error
 closeserial(port_TCL);
     case 0 // Derivative State Update


     case 1 // Output Update
      //DEBUG("Output update ");
      // FIXME: Mettre ici tout ce qui sert a ecrire sur le port serie

      //u1=block.inptr(1)
      u1=uint16(block.inptr(1));
      
//      if ( abs(u1)>2044 ) then 
//          u1=2044;
////      else
////         u1= uint16(u1);
//      end

//DEBUG(string(u1))
      //envoi de P + nombre sur 2 octets         
      values= "P" + ascii(int(u1/256)) + ascii(u1-int(u1/256)*256)

      writeserial(port_TCL,values);

      //reception des données
      [q,flags]=serialstatus(port_TCL)

       while(q<8)
         [q,flags]=serialstatus(port_TCL)
       end
       values=readserial(port_TCL);
       data=ascii(values)';

       y1(1)=data(1)*256+data(2); //position
       y1(2)=data(3)*256+data(4); //delta position
       y1(3)=data(5)*256+data(6);//deltaT
       y1(4)=data(7)*256+data(8);//tension
       y1=double(int16(y1));
       vit=y1(2)*10000/y1(3); //vitesse
            
      //block.outptr(1) =vit;
       block.outptr(1) =y1(1);
       
     case 2 // State Update

     case 3 // OutputEventTiming

     case 4 // Initialization

     case 5 // Ending
      // FIXME: quoi faire a la fin de la simulation

       values= "P" + ascii(0) + ascii(0)
       writeserial(port_TCL,values);
       closeserial(port_TCL);

     case 6 // Re-Initialisation

     case 9 // ZeroCrossing

    else // Unknown flag

    end
endfunction
