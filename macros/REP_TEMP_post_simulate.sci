//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010-2010 - DIGITEO - David Fournier
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function REP_TEMP_post_simulate(%cpr, scs_m, needcompile)

    if ~exists("mode_debug") then
        mode_debug = %f;
    else
        mode_debug = %t;
    end;

    // Récupérer les informations utiles des autres blocs
    presence_scope = %f;
    list_scope = [];
    grid_on = 0;
    tr_p = 0;
    tm_p = 0;
    tpic_p = 0;
    for i = 1 : size(scs_m.objs)
        curObj= scs_m.objs(i);
        if (typeof(curObj) == "Block" & curObj.gui == "SCOPE") then
            presence_scope = %t;
            list_scope($+1) = i;
        end;
        if (typeof(curObj) == "Block" & curObj.gui == "REP_TEMP") then
            grid_on = evstr(scs_m.objs(i).graphics.exprs(2));
            tr_p = evstr(scs_m.objs(i).graphics.exprs(3));
            tm_p = evstr(scs_m.objs(i).graphics.exprs(4));
            tpic_p = evstr(scs_m.objs(i).graphics.exprs(5));
        end;
    end;
    if ~presence_scope then
        disp("Avertissement : Vous faites une étude temporelle sans block SCOPE.");
    end;
    rapidite = autorisation_rapidite(tr_p, tm_p, tpic_p);


    // Parcourir les scopes pour en extraire les légendes
    legendes = cell();
    for i=1 : size(list_scope,1)
        j = list_scope(i);
        obj = scs_m.objs(j);
        nb_outputs($+1) = evstr(obj.graphics.exprs(1));
        legendes{i} = obj.graphics.exprs(2:$);
    end;

    //extraction des champs stockés
    nb_scope = size(list_scope,1);
    D = [];
    legend_c = [];



    handle_fig = figure();
    set(handle_fig,"background",8);

    for i=1 : nb_scope
        if ~mode_debug then
            drawlater();
        else
            clc;
        end;

        subplot(nb_scope,1,i);
        legend_c = legendes{i};

        list_obj = scs_m.objs(list_scope(i)).model.rpar.objs;
        no = 1;
        for j=1 : size(list_obj)
            if (typeof(list_obj(j)) == "Block" & list_obj(j).gui == "TOWS_c") then
                D(i,no) = evstr(list_obj(j).graphics.exprs(2));
                no = no+1;
            end;
        end;

        for no=1:nb_outputs(i) //on trace ensuite sur chaque sous figure la réponse
            plot(D(i,no).time,D(i,no).values,"color",couleur(no),"thickness",2);
        end;

        if grid_on>=1 then
            xgrid;
        end;

        h = legend(legend_c);
        set(h,"background",8);
        set(h,"legend_location","upper_caption");
        if rapidite then
            decalage = trouver_decalage();
            df_message("decalage : " + string(decalage));
            for no=1:nb_outputs(i)
                resultat = performance_rapidite(D(i,no), decalage, legend_c(no), tr_p, tm_p, tpic_p);
                df_message("Performance de rapidité de la courbe " + legend_c(no) + " : ");
                df_message(resultat);
                legend_c(no) = legend_c(no) + " (" + resultat.legende + ")";
                graphique_rapidite(resultat, no, tr_p, tm_p, tpic_p, grid_on == 2, nb_outputs(i)<2);
                affichage_rapidite(resultat, tr_p, tm_p, tpic_p);
            end;
            h = legend(legend_c);
            set(h,"background",8);
            set(h,"legend_location","upper_caption");
            set(h,"line_mode","off");
        end;

        ax = gca();
        for i=1 : size(ax.children,1)
            if size(ax.children(i).children) > 0 then
                ax.children(i).children.display_function = "formatTempTip";
                //ok = datatipInitStruct(ax.children(i).children,"formatfunction","formatTempTip");
            end;
        end;

        drawnow();
        if mode_debug then
            pause;
        end;
    end;
endfunction;

function str = formatTempTip(curve,pt,index)
    //this function is called by the datatips mechanism to format the tip
    //string for the magnitude bode curves
    str = msprintf("%.4g"+_("s")+"\n%.4g", pt(1),pt(2));
endfunction;

function boolean = autorisation_rapidite(tr_p, tm_p, tpic_p)
    boolean = %f;
    for i = 1 : size(scs_m.objs)
        curObj= scs_m.objs(i);
        if (typeof(curObj) == "Block" & curObj.gui == "STEP_FUNCTION") then
            boolean = %t;
            break;
        end;
    end;

    if (~boolean) & ((tr_p<>0) | (tm_p<>0) | (tpic_p<>0)) then
        disp("Avertissement : L''étude des performances de rapidité a été annulée car il n''y a pas de block STEP_FUNCTION.");
    end;
    boolean = boolean & ((tr_p<>0) | (tm_p<>0) | (tpic_p<>0));
endfunction

function out = trouver_decalage()
    compteur = 0;
    for i = 1 : size(scs_m.objs)
        curObj= scs_m.objs(i);
        if (typeof(curObj) == "Block" & curObj.gui == "STEP_FUNCTION") then
            compteur = compteur + 1;
            decalage(compteur) = evstr(curObj.model.rpar.objs(1).graphics.exprs(1));
        end;
    end;

    if compteur == 0 then
        out = -1
    elseif compteur == 1 then
        out = decalage(1);
    else
        out = decalage(1);
        for i = 1 : compteur
            if out <> decalage(compteur) then
                out = -1;
                break;
            end;
        end;
    end;
endfunction;

function matrice = performance_rapidite(donnees,decalage,legende,tr_p,tm_p,tpic_p)
    matrice.time = donnees.time;
    matrice.values = donnees.values;
    matrice.legende_originale = legende;

    //Nombre de points
    nbr_points = size(matrice.time,1);

    //Valeur initiale
    matrice.s_ini = matrice.values(1);

    //Valeur finale
    matrice.t_fin = matrice.time(nbr_points);
    matrice.s_fin = matrice.values(nbr_points);

    //Valeurs minimales et maximales
    matrice.s_min = min(matrice.values);
    matrice.s_max = max(matrice.values);

    //Sortie de la consigne
    saut_echelon = %t;
    for n = 1 : nbr_points
        if ~(matrice.values(n) == matrice.s_min | matrice.values(n) == matrice.s_max) then
            saut_echelon = %f;
            break;
        end;
    end;

    //Régime établi
    nbr_points_regime_etabli = int(10/100 * nbr_points);
    matrice.amplitude = matrice.s_fin - matrice.s_ini;
    matrice.s_fin_p = ~saut_echelon & matrice.amplitude <> 0;
    if ~matrice.s_fin_p then
        if saut_echelon then
            matrice.legende = "La réponse est la consigne saut échelon.";
        elseif matrice.s_min == matrice.s_max then
            matrice.legende = "La réponse ne varie pas.";
        else
            matrice.legende = "L''étude ne semble pas être faisable.";
        end;
    else
        matrice.legende = "La réponse ne semble pas avoir atteint le régime établi.";
    end;
    if  matrice.s_fin_p then
        for n = nbr_points - nbr_points_regime_etabli : nbr_points 
            s_t = matrice.values(n);
            ecart = s_t - matrice.s_fin;
            ecart_p = abs(ecart/matrice.amplitude)*100;
            if (ecart_p>0.5) then
                matrice.s_fin_p = %f;
                break;
            end;
        end;
    end;

    if matrice.s_fin_p then
        //Valeurs caractéristiques
        matrice.s_tr_plus = matrice.s_ini + (1 + abs(tr_p)/100) * matrice.amplitude;
        matrice.s_tr_moins = matrice.s_ini + (1 - abs(tr_p)/100) * matrice.amplitude;
        matrice.s_tm_plus = matrice.s_ini + (1 - abs(tm_p)/100) * matrice.amplitude;
        matrice.s_tm_moins = matrice.s_ini + (0 + abs(tm_p)/100) * matrice.amplitude;

        //Echelon montant
        ech_m = matrice.amplitude > 0;

        //performances
        tm_test = %t;
        matrice.tpic_p = %f;
        spic = 0;
        for n = 1 : nbr_points - nbr_points_regime_etabli
            s_t = matrice.values(n);
            t_t = matrice.time(n);
            t_t1 = matrice.time(n+1);
            ecart = s_t - matrice.s_fin;
            ecart_p = abs(ecart/matrice.amplitude)*100;
            amplitude_t = abs(s_t - matrice.s_ini);

            //décalage
            if (s_t == matrice.s_ini) then
                matrice.t_decalage = t_t;
            end;

            //temps de réponse à tr_p %
            if (ecart_p >= abs(tr_p)) then
                matrice.tr_fin = t_t1;
            end;

            //temps de montée à tm_p %
            if (ecart_p >= 100-abs(tm_p)) then
                matrice.tm_ini = t_t;
            end;
            if (tm_test & (ecart_p >= abs(tm_p)) & (((ecart < 0) & ech_m) | ((ecart > 0) & ~ech_m))) then
                matrice.tm_fin = t_t1;
            else
                tm_test = %f;
            end;

            //Temps du premier pic
            if ((amplitude_t > spic) & (amplitude_t > abs(matrice.amplitude))) then
                matrice.tpic_p = %t;
                spic = amplitude_t;
                matrice.spic = spic;
                matrice.tpic = t_t;
                matrice.ecart_pic_p = abs((s_t - matrice.s_fin)/matrice.amplitude)*100;
            end;
        end;

        if decalage == -1 then
            matrice.retard = 0;
            matrice.tr_ini = matrice.t_decalage;
            matrice.retard_legende = "retard non défini";
            retard_legende = "retard non défini";
        else
            matrice.retard = matrice.t_decalage - decalage;
            matrice.tr_ini = decalage;
            matrice.retard_legende = "retard de " + format_nombre(matrice.retard) + "s";
            if matrice.retard <> 0 then
                retard_legende = "retard=" + format_nombre(matrice.retard) + "s";
            else
                retard_legende = "";
            end;
        end;
        matrice.tr = matrice.tr_fin - matrice.tr_ini;
        matrice.tm = matrice.tm_fin - matrice.tm_ini;

        //legende
        matrice.tr_legende = "tr" + format_nombre(abs(tr_p)) + "%=" + format_nombre(matrice.tr) + "s";
        matrice.tm_legende = "tm" + format_nombre(abs(tm_p)) + "%=" + format_nombre(matrice.tm) + "s";
        if matrice.tpic_p then
            matrice.tpic_legende = "t1=" + format_nombre(matrice.tpic) + "s - D1=" + format_nombre(matrice.ecart_pic_p) + "%";
        end;

        matrice.legende = retard_legende;
        if tr_p <> 0 then
            if matrice.legende <> "" then
                matrice.legende = matrice.legende + " | ";
            end;
            matrice.legende = matrice.legende + matrice.tr_legende;
        end;
        if tm_p  <> 0 then
            if matrice.legende <> "" then
                matrice.legende = matrice.legende + " | ";
            end;
            matrice.legende = matrice.legende + matrice.tm_legende;
        end;
        if matrice.tpic_p then
            if matrice.legende <> "" then
                matrice.legende = matrice.legende + " | ";
            end;
            matrice.legende = matrice.legende + matrice.tpic_legende;
        end;
    end;
endfunction;

function matrice_couleur = couleur(nombre)
    //c_color = ["k", "b", "g", "r", "c", "m", "y"];
    c_color = ["black", "blue", "[0,0.5,0]", "red", "[0,0.75,0.75]", "magenta", "[0.75,0.75,0]"];
    matrice_couleur = c_color(modulo(nombre-1,7)+1);
    if length(matrice_couleur)>1 & ~isempty(grep(matrice_couleur, "[")) then
        matrice_couleur = evstr(c_color(modulo(nombre-1,7)+1));
    end;
endfunction;

function graphique_rapidite(donnees, k_couleur, tr_p, tm_p, tpic_p, grille, axe)
    if donnees.s_fin_p then
        if (tr_p <0) then
            if axe then
                plot(donnees.time,donnees.s_fin,"--","color",couleur(k_couleur));
                plot([donnees.tr_fin,donnees.t_fin],[donnees.s_tr_moins,donnees.s_tr_moins],":","color",couleur(k_couleur));
                plot([donnees.tr_fin,donnees.t_fin],[donnees.s_tr_plus,donnees.s_tr_plus],":","color",couleur(k_couleur));
                plot([donnees.tr_ini,donnees.tr_ini],[donnees.s_min,donnees.s_fin],":","color",couleur(k_couleur));
                plot([donnees.tr_fin,donnees.tr_fin],[donnees.s_min,donnees.s_fin],":","color",couleur(k_couleur));
            end;
            plot([donnees.tr_ini,donnees.tr_fin],[donnees.s_fin,donnees.s_fin],".","color",couleur(k_couleur));
            xstring(donnees.tr_fin,donnees.s_fin,donnees.tr_legende);
        end;
        if (tm_p <0) then
            if axe then 
                plot(donnees.time,donnees.s_fin,"--","color",couleur(k_couleur));
                plot([donnees.time(1),donnees.tm_ini],[donnees.s_tm_moins,donnees.s_tm_moins],":","color",couleur(k_couleur));
                plot([donnees.time(1),donnees.tm_fin],[donnees.s_tm_plus,donnees.s_tm_plus],":","color",couleur(k_couleur));
                plot([donnees.tm_ini,donnees.tm_ini],[donnees.s_min,donnees.s_tm_moins],":","color",couleur(k_couleur));
                plot([donnees.tm_fin,donnees.tm_fin],[donnees.s_min,donnees.s_tm_plus],":","color",couleur(k_couleur));
            end;
            plot([donnees.tm_ini,donnees.tm_fin],[donnees.s_tm_moins,donnees.s_tm_moins],".","color",couleur(k_couleur));
            xstring(donnees.tm_fin,donnees.s_tm_moins,donnees.tm_legende);
        end;
        if (tpic_p <0 & donnees.tpic_p) then
            if axe then
                plot(donnees.time,donnees.s_fin,"--","color",couleur(k_couleur));
                plot([donnees.tpic,donnees.tpic],[donnees.s_min,donnees.s_fin],":","color",couleur(k_couleur));
            end;
            plot([donnees.tpic,donnees.tpic],[donnees.s_fin,donnees.s_max],".","color",couleur(k_couleur));
            xstring(donnees.tpic,donnees.s_max,donnees.tpic_legende);
        end;
        if grille & axe then
            xgrid(8);
        end;
    end;
endfunction

function affichage_rapidite(donnees, tr_p, tm_p, tpic_p)
    disp("Performance de rapidité de la courbe "" " + donnees.legende_originale + " "" (la précision est de " + format_nombre(donnees.time(2)) + "s) :");
    if donnees.s_fin_p then
        if tr_p <>0 then
            disp("   Le temps de réponse à " + format_nombre(abs(tr_p)) + "% est de " + format_nombre(donnees.tr) + "s avec un " + donnees.retard_legende + ".");
        end;
        if tm_p <>0 then
            disp("   Le temps de montée à " + format_nombre(abs(tm_p)) + "% est de " + format_nombre(donnees.tm) + "s.");
        end;
        if tpic_p <>0 & donnees.tpic_p then
            disp("   L''instant du premier dépassement de " + format_nombre(donnees.ecart_pic_p) + "% est à " + format_nombre(donnees.tpic) + "s.")
        end;
    else
        disp("   " + donnees.legende);
    end;
endfunction;

function str = format_nombre(nombre)
    chiffre_significatif = 4;
    str = msprintf("%." + string(chiffre_significatif) + "g", nombre);
endfunction;

function df_message(texte)
    if mode_debug then
        disp(texte);
    end;
endfunction;
