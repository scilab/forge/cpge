//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010-2010 - DIGITEO -
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function scs_m=adapt_scope(scs_m,num_pts)

       //adaptation des blocs scope
        nb_scope=0
        	nb_outputs = [];
	nb_total_outputs = 0;
        for i=1 : size(scs_m.objs)
		obj = scs_m.objs(i);
		if typeof(obj) == "Block" then
			if obj.gui == "SCOPE" then
				nb_scope = nb_scope + 1;
				nb_outputs(nb_scope) = evstr(scs_m.objs(i).graphics.exprs(1));
				//recherche parmi les blocs le tows
				list_obj = scs_m.objs(i).model.rpar.objs;
				no = 1;
				for j=1 : size(list_obj)
					if (typeof(list_obj(j)) == "Block" & list_obj(j).gui == "TOWS_c") then //on affecte un nom pour le stockage dans scilab
						scs_m.objs(i).model.rpar.objs(j).graphics.exprs = [string(num_pts);"o"+string(no+nb_total_outputs);"0"];
						scs_m.objs(i).model.rpar.objs(j).model.ipar = [num_pts;2;24;no+nb_total_outputs];
						no=no+1;
					end
				end
				nb_total_outputs = nb_total_outputs + nb_outputs(nb_scope);
			end

		end
	end
            
endfunction
