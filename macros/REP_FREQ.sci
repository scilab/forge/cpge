//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=REP_FREQ(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'plot' then
        // deprecated
    case 'getinputs' then
        // deprecater
    case 'getoutputs' then
        // deprecated
    case 'getorigin' then
        // deprecated
    case 'set' then
        x=arg1;
        graphics=arg1.graphics;
        exprs=graphics.exprs
        model=arg1.model;
        cancel=%f
        
        previous_marges=model.rpar(4);
        previous_asymp=model.rpar(5);
        if size(model.rpar,2)==6 then
            previous_sym=model.rpar(6)
        else
            previous_sym=0
        end
        
        //Choix du type de diagramme
        while (%t & ~cancel)  do

            [ok,diag_type,inputsignal,outputsignal,marges,asymp, exprs]=scicos_getvalue('Etude harmonique',..
            ['Type de diagramme (1 pour Bode, 2 pour Black, 3 pour Nyquist, 4 pour lieu d''Evans)';'Grandeur(s) physique(s) d''entrée (si plusieurs les séparer par des ;)';'Grandeur(s) physique(s) de sortie';'Affichage des marges : oui(1)/non(0)';'Affichage des asymptotes : oui (1)/non(0)'], ..
            list("vec",1,'str',-1,'str',-1,'vec',1,'vec',1),[string(model.ipar);model.opar(1);model.opar(2);string(previous_marges);string(previous_asymp)])
            mess=[];
            if ~ok then
                // Cancel
                cancel=%t;   
                break;
            end
            if diag_type <> 1 & diag_type <> 2 & diag_type <> 3 & diag_type <> 4
                mess=[mess ;_("Type of diagram must be 1, 2, 3 or 4")]
                ok = %f
            end
            if (marges ~= 1 & marges ~= 0) then
                mess=[_("You have to type 1 or 0 for displaying marges")]
                ok = %f
            end
            if (asymp ~= 1 & asymp ~= 0) then
                mess=[_("You have to type 1 or 0 for displaying asymptotes")]
                ok = %f
            end
            
            
            
            if ok then
                model.ipar=diag_type
                select diag_type
                case 1 then
                    Nom="Bode"
                case 2 then
                    Nom="Black"
                case 3 then 
                    Nom="Nyquist"
                case 4 then
                    Nom="Evans"
                end
                graphics.exprs=Nom;
                model.opar=list(inputsignal,outputsignal)
                model.rpar(4)=marges;
                model.rpar(5)=asymp;

                x.model=model;
                x.graphics=graphics;
                break;
            else
                message(mess);
            end
        end
        
        //prise en compte de la symetrie pour Nyquist
            if diag_type==3 then
                exprssym="0"
                sym=0
                while (%t & ~cancel)  do

                    [ok2,sym, exprssym]=scicos_getvalue('Option spécifique Nyquist',..
                    ['Pour le tracé de Nyquist, choisir si la symétrie doit être faite (1) ou non (0)'], ..
                    list("vec",1),string(previous_sym))
                    mess=[];
                    if ~ok2 then
                        // Cancel
                        cancel=%t;   
                        break;
                    end
                    if (sym ~= 1 & sym ~= 0)  then
                        mess=[_("Entrer 0 ou 1")]
                        ok2 = %f
                    end
                    
                    if ok2 then
                        model.rpar(6)=sym 
                        x.model=model;
                        break
                    else
                        message(mess);
                    end
               end
            end
        
        //Choix des entrée et sortie pour calculer la FT
//        while (%t & ~cancel) do
//            [ok2,inputsignal,outputsignal, exprs]=scicos_getvalue('Définir les noms des grandeurs physiques d''entrée et de sortie',..
//            ['Grandeur d''entrée';'Grandeur de sortie'], ..
//            list("str",-1, "str", -1),model.opar)
//            mess=[];
//
//            if ~ok2 then
//                // Cancel
//                cancel=%t; 
//                break;
//            end
//
//            if ok2 then
//                model.opar=list(inputsignal,outputsignal)
//                x.model=model;
//                break
//            else
//                message(mess);
//            end
//
//        end

        // recupération des fréquences mini, maxi et nombre de points
        while (%t & ~cancel) do
            mess=[]
//            if exists("model.rpar(3)") then num_wi=string(model.rpar(3));
//            else num_wi="";
//            end
//            disp(num_wi)
            num_wi=model.rpar(3)
            [ok3,wmin,wmax,num_w,exprs]=scicos_getvalue('Bornes fréquentielles',..
            ['wmin (rad/s)';'wmax (rad/s)';'Nombre de points'], ..
            list("vec",1, "vec", 1,"vec",1),[string(model.rpar(1));string(model.rpar(2));string(num_wi)])
            mess=[];

            if ~ok3 then
                // Cancel
                cancel=%t 
                break;
            end

            if (~isempty(num_w) & num_w<>int(num_w))
                mess=[_("Le nombre de points doit être un entier positif")]
                ok3 = %f
            end

            if wmin<=0 | wmax<=0
                mess=[_("Les pulsations doivent être positives")]
                ok3 = %f
            end

            if wmin>=wmax
                mess=[_("wmax must be greater than wmin")]
                ok3 = %f
            end

            if ok3 then
                model.opar=list(inputsignal,outputsignal) 
                model.rpar(1)=wmin;
                model.rpar(2)=wmax;
                model.rpar(3)=num_w;
                x.model=model;
                break
            else
                message(mess);
            end

        end
//
//        //selection de l'option de tracé des marges de phase et gain
//        while (%t & ~cancel) do
//            [ok4,marges, exprs]=scicos_getvalue('Affichage des marges',..
//            ['Oui (1) / Non (0)'], ..
//            list("vec",1),string(previous_marges))
//            mess=[];
//
//            if ~ok4 then
//                // Cancel
//                return;
//            end
//
//            if (marges ~= 1 & marges ~= 0) then
//                mess=[_("Vous devez taper 1 ou 0 pour afficher les marges")]
//                ok4 = %f
//            end
//
//            if ok4 then
//                model.rpar(4)=marges;
//                x.model=model;
//                break
//            else
//                message(mess);
//            end
//
//        end

    case 'define' then
        model=scicos_model();
        k=1;
        model.sim=list("REP_FREQ_sim",99) // 99 type blocks are ignored by simulator.
        model.blocktype='c';
        model.dep_ut=[%f %f];
        model.in=[];
        model.intyp=[];
        model.out=[];
        model.outtyp=[];
        model.rpar=[1e-2,1e2,1000,0,0,0];
        model.ipar=[1];
        model.opar=list("E","S")
        x=standard_define([2 2],model,[],[]);
        x.graphics.in_implicit=[];
        x.graphics.out_implicit=[];
        x.graphics.style=["blockWithLabel;verticalLabelPosition=center;displayedLabel= %s"]
        x.graphics.exprs="Bode";

    end

endfunction
