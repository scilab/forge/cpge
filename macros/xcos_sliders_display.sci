//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [scs_m,err]=xcos_sliders_display(scs_m)
    global xcos_sliders figure_xcos_sliders
    err=0
    //Recherche des boutons dans le schema bloc et extraction des noms pour créer la structure xcos_sliders
     rep=1
     xcos_sliders.names=[]
     xcos_sliders.value_min=[]
     xcos_sliders.value_max=[]
     xcos_sliders.value_ini=[]
     xcos_sliders.nb_steps=[]
     for i=1:size(scs_m.objs)
        obj=scs_m.objs(i);
        if typeof( obj )=='Block' & obj.gui=='XCOS_SLIDER' then
           xcos_sliders.names(rep)=obj.graphics.exprs(1)
           xcos_sliders.value_max(rep)=evstr(obj.graphics.exprs(2))
           xcos_sliders.value_min(rep)=evstr(obj.graphics.exprs(3))
           xcos_sliders.value_ini(rep)=evstr(obj.graphics.exprs(4))
           xcos_sliders.nb_steps(rep)=evstr(obj.graphics.exprs(5))
           scs_m.objs(i).model.rpar(5)=rep 
           scs_m.objs(i).model.opar=list(obj.graphics.exprs(1))
           rep=rep+1
        end    
    end
    
//    xcos_sliders.names=['slider1']
//    xcos_sliders.value_max=[100]
//    xcos_sliders.value_min=[0]
//    xcos_sliders.value_ini=[0]
//    xcos_sliders.nb_steps=[100]


    if size(xcos_sliders.names,'*')>8 then
            message("Le nombre de sliders est limité à 8")
            err=1
            return ;
    elseif size(unique(xcos_sliders.names),'*')<>size(xcos_sliders.names,'*') then
            message("Vous devez donner des noms différents aux sliders")
            err=1
            return ;
    end
    
    // Effacement de la fenêtre des boutons si nécessaire
    //exist_fig=0
    list_fig=winsid()
    for i=list_fig
        h=scf(i)
        if h.Figure_name=='Xcos Sliders' then
            //exist_fig=1
            delete(h)
        end
    end

    //création de la fenêtre des boutons à la bonne taille (fonction du nombre de boutons)
    nb_xcos_sliders=size(xcos_sliders.names,'*')
    figSizeMax=nb_xcos_sliders*(18+30)
    //if ~exist_fig then
        figure_xcos_sliders = figure('Figure_name','Xcos Sliders','BackgroundColor',[1 1 1],'Position',[10,10,400,figSizeMax]);
        setlanguage('fr_FR')
        delmenu(figure_xcos_sliders.figure_id, gettext("File"));
        delmenu(figure_xcos_sliders.figure_id, gettext("Tools"));
        delmenu(figure_xcos_sliders.figure_id, gettext("Edit"));
        delmenu(figure_xcos_sliders.figure_id, gettext("?"));
        toolbar(figure_xcos_sliders.figure_id, "off");
    //end
    //clf(figure_xcos_display)

    // fonction permettant de remplir la figure f contenant les boutons xcos en spécifiant leur position dans la fenêtre (numero de placement de 1 à 8) et leur nom
    figSize = figure_xcos_sliders.axes_size;
    xcos_sliders.sliders=list()
    xcos_sliders.textlabel=list()
    xcos_sliders.textvalue=list()

    for i=1:nb_xcos_sliders
        position=[10 figSize(2)-i*(18+20) 150 20]
        position_textvalue=[180 figSize(2)-i*(18+20) 80 20]
        text_position=[10 figSize(2)-i*(18+20)-14 150 18]
        
        step=(xcos_sliders.value_max(i)-xcos_sliders.value_min(i))/xcos_sliders.nb_steps(i)

        xcos_sliders.textlabel(i)=uicontrol("Parent", figure_xcos_sliders,"units", "pixels",'BackgroundColor',[1 1 1], "Style", "text","Position", text_position,"String","textlabel");
        xcos_sliders.textlabel(i).String= xcos_sliders.names(i)
        xcos_sliders.textvalue(i)=uicontrol( "Parent", figure_xcos_sliders,"Style", "text",'HorizontalAlignment','left','BackgroundColor',[1 1 1], "Position", position_textvalue,'String','valeur = '+string(xcos_sliders.value_ini(i)));
        xcos_sliders.sliders(i)=uicontrol( "Parent", figure_xcos_sliders,"units", "pixels","Style", "slider", "min",xcos_sliders.value_min(i),"max",xcos_sliders.value_max(i),'value',xcos_sliders.value_ini(i),'SliderStep',[step step*10],"Position", position, "callback" , "xcos_sliders_callback(xcos_sliders,"+string(i)+")");
    end

endfunction


