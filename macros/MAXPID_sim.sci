//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function block=MAXPID_sim(block,flag)
  global bras6 bras
  global moteur ecrou
  global data_bras_init6 data_bras_init data_mot_init
  // Declare some local (to the simulation function) but reused variables
  xdata_mot_init = [0,1.5,1.5,0,0;1.5,9,9,1.5,1.5;9,14,14,9,9;14,14.5,14.5,14,14;14.5,16,16,14.5,14.5;16,32.5,32.5,16,16;32.5,33.5,33.5,32.5,32.5];
  ydata_mot_init = [1,1,-1,-1,1;1.75,1.75,-1.75,-1.75,1.75;2.5,2.5,-2.5,-2.5,2.5;2.25,2.25,-2.25,-2.25,2.25;2.5,2.5,-2.5,-2.5,2.5;0.6,0.6,-.6,-.6,.6;2,2,-2,-2,2];
  data_ecrou_init = [-1.75,-1.75,1.75,1.75,1.75;-2.25,2.25,2.25,-2.25,-2.25];
  xbras_init = 7 + [0, 21, 21, 0;
                    21, 5.5 + 21, 5.5 + 21, 21;
                    5.5 + 21, 5.5 + 21, 1.2 + 5.5 + 21, 1.2 + 5.5 + 21;
                    1.2 + 5.5 + 21, 2.4 + 5.5 + 21, 2.4 + 5.5 + 21, 1.2 + 5.5 + 21;
                    2.4 + 5.5 + 21, 1.5 + 2.4 + 5.5 + 21, 1.5 + 2.4 + 5.5 + 21, 2.4 + 5.5 + 21];
  ybras_init =[
      1.5,1.5,-1.5,-1.5;
      0.6,0.6,-0.6,-0.6;
      -5,5,5,-5;
      5,5,-5,-5;
      1,1,-1,-1];

  if flag == 4 | flag == 6
// Initialisation || Re-Init
// if already exists (stopped) then reuse
    f = findobj("Tag", block.uid);
    if f <> [] then
      return;
    end

// Create an empty figure without Menu's nor toolbar
    f = figure("Tag", block.uid, "Figure_name", "MAXPID","figure_position",[0,0]);
    f.background=-2;
// delete standard menus
    delmenu(f.figure_id, gettext("&File"));
    delmenu(f.figure_id, gettext("&Tools"));
    delmenu(f.figure_id, gettext("&Edit"));
    delmenu(f.figure_id, gettext("&?"));
    toolbar(f.figure_id, "off");

    a=gca();
    drawlater();
    
// On force à mettre la même échelle en x et y
    a.isoview="on"
// Définition des dimensions de la zone de tracé
    a.data_bounds=[-35,-12;45,36]
// ??
    a.axes_bounds=[0,0,1,1]
// On place des marges entre le cadre de tracé et le bord de la fenetre
    a.margins=[0.02 0.02 0.02 0.02 ]

    gcf().color_map = [0,0,0;0.58,0.58,0.58;1,0.67,0.09;0.28,0.28,0.28;14/255,131/255,214/255;255,255,255;180/255,68/255,35/255;237/255,182/255,0;0.2,0.2,0.2;228/255,152/255,20/255];
    gca().line_style = 1;

    //Tracé de la boite
    gca().foreground = 3;//boite général
    xfrect(-30.5,33,16,40)
    xfrect(-14.5,33,21.5,40)
    xfarc(-26,33,66,66,0*64,90*64)
    xfrect(7,0,33,7)
    xfrect(-30.5,-7,70.5,2)

    gca().foreground = 10;
    xfarc(-24,31,62,62,0*64,90*64)
    xfpoly([-12.5,-12.5,7,7],[-7,31,31,-7])
    xfpoly([7,7,38,38],[-7,0,0,-7])

    gca().foreground = 1;//triangle
    xfpoly([-28,-26,-24],[4.6,8,4.6],1)
    xfpoly([-27.7,-26,-24.3],[4.8,7.7,4.8],8)

    gca().foreground = 7;//bouton rouge
    xfarc(-30,13,4,4,0,360*64)

    gca().foreground = 1;//contour
    xarc(-26,33,66,66,0*64,90*64)
    xrect(-30.5,33,16,40)
    xpoly([-14.5,7],[33,33])
    xpoly([-14.5,40,40],[-7,-7,0])
    xrect(-30.5,-7,70.5,2)

    xarc(-24,31,62,62,0*64,90*64)
    xpoly([-12.5,-12.5,7],[-7,31,31])
    xpoly([38,38],[0,-7])

    //poignée
    xfrect(-3,33,11,2.5)

    //carte de commange
    xfrect(-29.5,31,14,17)
    gca().foreground = 5;
    xfrect(-29,30.5,13,16)
    gca().foreground = 6;
    xfrect(-22,29.5,5,10)
    set(gca(), "font_style", 8, "font_size", 2);
    xstring(-26,0,"MAXPID")

    //graduation
    gca().foreground = 9;
    xfarc(-15,22,44,44,0*64,100*64)
    gca().foreground = 10;
    xfarc(-12,19,38,38,0*64,100*64)

    //attache plastique bati
    gca().foreground = 1;
    xfrect(-12,-6,3,1.5)
    xfrect(-9,-7,3,1.5)
    xfrect(34,-6,3,1.5)
    xfrect(31,-7,3,1.5)


// On récupère les coordonnées des bords de la zone de tracé
    gca().foreground = 1;
    rect=matrix(a.data_bounds',-1,1)
// On trace un rectangle le long des bords de la zone de tracé
    xpoly(rect([1 3 3 1]),rect([2,2,4,4]),'lines',1)



    //Tracé du moteur + vis
    gca().foreground = 1;
    xfpolys(xdata_mot_init',ydata_mot_init',[1,1,2,2,2,4,2])
    moteur=gce();
    //moteur.tag="MAXPID#"+block.label+"#moteur";
    move(moteur,[-11.5,8])
    data_mot_init=cell(size(moteur.children));
    for i=1:size(moteur.children,1)
      data_mot_init{i} = moteur.children(i).data;
    end

    //Tracé de l'écrou
    gca().foreground = 1;
    xfpoly(data_ecrou_init(1,:),data_ecrou_init(2,:),2)
    ecrou=gce()

// Tracé du bras et stockage de l'objet dans "bras"
// ligne moyenne de 0 à 30 suivant X, largeur 3cm
// décalé en X de 7cm vers la droite
    gca().foreground = 1;
    xfpolys(xbras_init',ybras_init',[1,2,2,2,2])
    bras=gce();
    data_bras_init=cell(size(bras.children));
    for i=1:size(bras.children,1)
      data_bras_init{i} = bras.children(i).data;
    end

    xrpoly([7+8,0],20,1.5,0);
    bras6=gce();
    data_bras_init6=bras6.data';
    delete(bras6);
    xfpoly(data_bras_init6(1,:),data_bras_init6(2,:),2);
    bras6=gce();



    // Tracé des axes et de la courbe et stockage de l'objet dans "courbe"
    //      gca().foreground = 1;
    //      xpoly([30,30,44],[43,30,30],'lines',0)
    //      xpoly([30],[30],'lines',0);
    //courbe=gce();

    // Tracé du bati
    gca().foreground = 3;
    xfpoly([-1.5,-2,-2,3.5,3.5,2,1.5],[8,0,-7,-7,0,0,8])
    xfarc(3.5,3.5,7,7,0,360*64)

    gca().foreground = 1;
    xpoly([-1.5,-2,-2,3.5],[8,0,-7,-7]);
    xpoly([3.5,2,1.5],[0,0,8])

    gca().foreground = 10;
    xfarc(2,4,3,6,170*64,100*64)
    gca().foreground = 1;
    xarc(2,4,3,6,180*64,90*64)

    gca().foreground = 3;
    xfpoly([10.5,11,3,3.5],[0,-7,-7,0])
    gca().foreground = 1;
    xarc(3.5,3.5,7,7,0,360*64)
    xpoly([10.5,11,3,3.5],[0,-7,-7,0])

    gca().foreground = 2;
    xfarc(-1.5,9.5,3,3,0,360*64)
    xfarc(4.5,2.5,5,5,0,360*64)

    gca().foreground = 1;
    xarc(-1.5,9.5,3,3,0,360*64)
    xarc(4.5,2.5,5,5,0,360*64)
    drawnow();
  end
  if flag ==  1
    // Output update
    u1=block.inptr(1);
    a = 7
    b = 8
    c = 8
    alpha=-atan((b-c*sin(u1))/(c*cos(u1)+a));

    f = findobj("Tag", block.label);
    drawlater()
    // rotation du bras de l'angle theta, autour de la pivot en (7,0)
    for i=1:size(bras.children,1)
       bras.children(i).data=rotate(data_bras_init{i}',u1,[7,0])'
    end
    bras6.data=rotate(data_bras_init6, u1,[7,0])'

    // rotation du moteur + vis de l'angle alpha, autour de la pivot en (0,8)
    for i=1:size(moteur.children,1)
       moteur.children(i).data=rotate(data_mot_init{i}',alpha,[0,8])'
    end

    // rotation de l'écrou de l'angle theta, autour de la pivot en (0,0)
    // puis translation là ou il faut : je voulais faire scaling mais pas réussi.
    data_ecrou=rotate(data_ecrou_init,alpha,[0,0]);
    xyecrou = rotate([15;0], u1,[7,0])';
    data_ecrou(1,:)=data_ecrou(1,:)+xyecrou(1);
    data_ecrou(2,:)=data_ecrou(2,:)+xyecrou(2);
    ecrou.data=data_ecrou';

  //  courbe.data=[courbe.data; 30+t, 16+u1]
     drawnow();
  end

endfunction
