//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
function PIC_init_serial_com()
    global connected port_TCL isreceiv y1_test inc t_prec
    t_prec =-1;
    if (connected == 1) then
        //Envoie d'une demande de démarrage de travail
        values=readserial(port_TCL);
        nbpoint = 1000;
        writeserial(port_TCL,"T"+ascii((int(evstr(nbpoint)/256))) + ascii(modulo(evstr(nbpoint),256)));
        //Attente de la reception de la trame de DEBUT
        values="";
        while (length(strindex(values,"BEG;")) == 0 )
            sleep(10);
            values=values+readserial(port_TCL);
            //disp(values)
        end
        disp('Initialisation terminée')
    else
        messagebox('Mode non connecté')
    end
    isreceiv = 0;
    y1_test = 0;
    //disp('inc '+string(inc));
    inc = 0;
endfunction