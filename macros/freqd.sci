//En attendant que le bug de iodelay soit corrigé

function  rf=freqd(numh,denh,D,s)
  rf=freq(numh,denh,s);
  for k=1:size(D,'*')
    rf(k,:)=rf(k,:).*exp(-D(k)*s)
  end
endfunction
